@extends('admin.index')


@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>New Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>

              <p>Bounce Rate</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->



        <section class="col-lg-8 connectedSortable">
          @if($errors->any())
          <div class="alert alert-danger">
          <h4>{{$errors->first()}}</h4>
          </div>
          @endif
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <div class="panel panel-default">
            <div class="panel-heading">users lists 
               <a class="btn btn-sm btn-success"  style="float: right;" href="{{route('CreateUsers')}}"><i class="fa fa-plus"></i> create user</a>
            </div>
            <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>name</th>
                  <th>email</th>
                  <th>roles</th>
                  <th>posts</th>
                </tr>
              </thead>

              @foreach($users as $user)
              <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->roles}}</td>
                <td>{{ count($user->post) }}</td>
                <td><a href="/admin/users/{{$user->id}}/edit" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> edit</a>
                </td>
                <td>
                <form action="{{route('DeleteUsers',['id'=>$user->id])}}" method="POST">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                  <input class="btn btn-danger" value="delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');">
                </form></td>
                @endforeach()
              </tr>
          

            </table>

            <a href="/admin" class="btn btn-warning">
              back
            </a>
          </div>
          </div>
          </div>
          <!-- /.nav-tabs-custom -->
</div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

  </div>






  @endsection()
