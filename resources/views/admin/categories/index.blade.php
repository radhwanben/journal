@extends('admin.index')


@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>New Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>

              <p>Bounce Rate</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->



        <section class="col-lg-8 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <div class="panel panel-default">
            <div class="panel-heading" style="text-align: right;">قائمة المجموعات  
               <a class="btn btn-sm btn-success"  style="float: left;" href="{{route('AdminCreateARCategory')}}"><i class="fa fa-plus"></i> إضافة مجموعة </a>
            </div>
            <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th colspan="2" style="text-align: right;">اعدادت </th>
                  <th  style="text-align: right;">عدد المقالات </th>
                  <th  style="text-align: right;">إسم المجموعة </th>
                </tr>
              </thead>
              <tr>
                @foreach($cateories as $cateorie)
                            <td>
                  <form action="{{route('AdmindeleteARCategory',['id'=>$cateorie->id])}}" method="POST">
                      {{ csrf_field() }} {{ method_field('DELETE') }}
                  <input class="btn btn-danger" value="حذف" type="submit" onclick="return confirm('هل تريد حقاً حذف هذا المقال  ?');">
                </form>  
              </td>
              <td style="text-align: right;">
                  <a href="{{url('admin/arabic/category/'.$cateorie->id.'/edit')}}" class="btn btn-warning btn-sm">edit</a>
              </td>

                <td style="text-align: right;">{{$cateorie->postar->count()}}</td>
                <td style="text-align: right;">{{$cateorie->name}}</td>
              </tr>

              @endforeach()
            </table>
          </div>
          </div>
          </div>
          <!-- /.nav-tabs-custom -->
</div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

  </div>






  @endsection()
