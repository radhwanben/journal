@extends('admin.index')


@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>New Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>

              <p>Bounce Rate</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->



        <section class="col-lg-8 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
                            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <div class="panel panel-default">
            <div class="panel-heading">users lists 
               <a class="btn btn-sm btn-success"  style="float: right;" href=""><i class="fa fa-plus"></i> create user</a>
            </div>
                <div class="panel-body">
                                         <form action="{{route('StoreEnPost')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group col-md-3">
                        <label for="title">Post Title</label>
                        <input type="text" class="form-control" name="title" placeholder="Write Post Title">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="Slug">Slug</label>
                        <input type="text" class="form-control" name="slug" placeholder="Write Slug">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="title">categories</label>
                        <select class="form-control form-control-lg" name="id_categories">
                            @foreach($categories as $categorie)
                            <option value="{{$categorie->id}}">{{$categorie->name}}</option>
                            @endforeach()
                        </select>
                    </div>
                    <div class="form-group col-md-3">

                        <label>Upload Image</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                        <span class="btn btn-default btn-file">
                            Browse… <input type="file" name="file" id="imgInp">
                        </span>
                            </span>
                            <input type="text" class="form-control" name="file" readonly>
                        </div>
                        <img id='img-upload' />
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputFile">Post Body</label>
                        <textarea class="form-control" rows="5" placeholder="Write Post" name="body"></textarea>
                    </div>
                    <div class="col-md-3">
                      <label for="publish">publish</label>
                       <select class="form-control" name="posted">
                            <option value="0">unpublish</option>
                            <option value="1">publish</option>
                          </select>
                    </div>
                      <div class="col-md-3">
                      <label for="mostreader">most reader</label>
                       <select class="form-control" name="mostreader">
                            <option value="0">no</option>
                            <option value="1">most reader</option>
                          </select>
                    </div>
                      <div class="col-md-3">
                      <label for="urgent">urgent</label>
                       <select class="form-control" name="urgent">
                            <option value="0">no</option>
                            <option value="1">urgent</option>
                          </select>
                    </div>
                    <div class="col-md-3">
                      <label for="notreader">not reader</label>
                       <select class="form-control" name="notreader">
                            <option value="0">reader</option>
                            <option value="1">notreader</option>
                          </select>
                    </div>


                    <div class="form-group col-md-2"><br>
                        <button type="submit" class="btn btn-success">Create</button>
                        <a class="btn btn-primary" href="{{route('ShowEnPosts')}}">back</a>
                    </div>
                </form>
                </div>
          <!-- /.nav-tabs-custom -->
</div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->






  @endsection()
