<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/photo/{{Auth::user()->photo}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a>
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>


        </li>



        <li class="treeview">
          <a href="{{route('ShowUsers')}}">
            <i class="fa fa-users" aria-hidden="true"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('ShowUsers')}}"><i class="fa fa-eye"></i>show users list</a></li>
          </ul>
        </li>



       <li class="treeview">
          <a href="#">
            <i class="fa fa-edit" aria-hidden="true"></i>
            <span>Posts English</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('ShowEnPosts')}}"><i class="fa fa-eye"></i> show english posts </a></li>

          </ul>
        </li>


      <li class="treeview">
          <a href="#">
            <i class="fa fa-edit" aria-hidden="true"></i>
            <span> Posts Arabic </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('ShowArPosts')}}"><i class="fa fa-eye"></i>show arabic posts</a></li>
          </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-list" aria-hidden="true"></i>
            <span>Categories English</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('AdminShowENCategory')}}"><i class="fa fa-eye"></i>show english category</a></li>

          </ul>
        </li>


      <li class="treeview">
          <a href="#">
            <i class="fa fa-list" aria-hidden="true"></i>
            <span>Categories Arabic</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('AdminShowARCategory')}}"><i class="fa fa-eye"></i>show arabic category</a></li>
          </ul>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-list" aria-hidden="true"></i>
            <span>SubCategories English</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-edit"></i>??</a></li>

          </ul>
        </li>


      <li class="treeview">
          <a href="#">
            <i class="fa fa-edit" aria-hidden="true"></i>
            <span>SubCategories Arabic</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-edit"></i>Suppliers Profile Builder</a></li>

            <li><a href="#"><i class="fa fa-edit"></i>Contractors Profile Builder</a></li>

          </ul>
        </li>


       


      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
