@extends('admin.index')


@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>150</h3>

              <p>New Orders</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>

              <p>Bounce Rate</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>2</h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{route('ShowUsers')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->



        <section class="col-lg-8 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <div class="panel panel-default">
            <div class="panel-heading">English Posts list 
               <a class="btn btn-sm btn-success"  style="float: right;" href="{{route('CreateArPosts')}}"><i class="fa fa-plus"></i> Create English Post</a>
            </div>
            <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>photo</th>
                  <th>title</th>
                  <th>slug</th>
                  <th>category</th>
                  <th>user</th>
                </tr>
              </thead>

              @foreach($posts as $post)
              <tr>
                <td><img src="/storage/{{$post->photo}}" style="    width: 55px;
    height: 55px;"></td>
                <td>{{$post->title}}</td>
                <td>{{$post->slug}}</td>
                <td>{{$post->categoryar['name']}}</td>
                <td>{{$post->user->name}}</td>
                <td><a href="{{url('admin/arabic-posts/'.$post->id.'/edit')}}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> edit</a>
                </td>
                <td>
                <form action="{{route('DeleteArPosts',['id'=>$post->id])}}" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                  <input class="btn btn-danger" value="delete" type="submit" onclick="return confirm('are you sure you want delete this post  ?');">
                </form>
                </td>  
                @endforeach()
              </tr>
          

            </table>


          </div>

          </div>
                      <div class="text-right">
              {!! $posts->links() !!}
            </div>
          </div>
          <!-- /.nav-tabs-custom -->
</div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

  </div>






  @endsection()
