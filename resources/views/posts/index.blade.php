@extends('layouts.master')

@section('content')

    <!-- ##### Hero Area End ##### -->
   <div class="hero-area owl-carousel">
        <!-- Single Blog Post -->
        @foreach($urgposts as $urgpost)
        <div class="hero-blog-post bg-img bg-overlay" style="background-image: url('/storage/{{$urgpost->photo}}');">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Post Contetnt -->
                        <div class="post-content text-center">

                            <a href="post/{{$urgpost->slug}}" class="post-title" data-animation="fadeInUp" data-delay="300ms">{{$urgpost->title}}</a>
                            <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                <a href="/ar/categories/{{optional($urgpost->categoryar)->id}}">{{optional($urgpost->categoryar)->name}}</a>
                            </div>
                            <a href="post/{{$urgpost->slug}}" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach()

    </div>
    <!-- ##### Hero Area End ##### -->
</div>


   

    <!-- ##### Mag Posts Area Start ##### -->
    <section class="mag-posts-area d-flex flex-wrap">
        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Left Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area left-sidebar mt-30 mb-30 bg-white box-shadow">
            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->

        </div>



        <!-- >>>>>>>>>>>>>>>>>>>>
             Main Posts Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="mag-posts-content mt-30 mb-30 p-30 box-shadow">
            <!-- Trending Now Posts Area -->

                   <div class="feature-video-posts mb-30">
                <!-- Section Title -->
            <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 >Libiya </h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postlibiya as $postlibiy)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postlibiy->photo}}" alt="">
                        <div class="post-content">
                            <a href="/post/{{$postlibiy->slug}}" class="post-title">{{$postlibiy->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div>
            </div>




            <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 >Arab World   </h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postsnewsar as $postsnewsa)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postsnewsa->photo}}" alt="">
                        <div class="post-content">
                            <a href="/post/{{$postsnewsa->slug}}" class="post-title">{{$postsnewsa->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div>
            </div>

            <!-- Feature Video Posts Area -->
            <!-- Feature Video Posts Area -->
     
                 <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 >world   </h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postsnews as $postsnew)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postsnew->photo}}" alt="">
                        <div class="post-content">
                            <a href="/post/{{$postsnew->slug}}" class="post-title">{{$postsnew->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div>
            </div>
                
                <div class="section-heading">
                    <h5 >Business </h5>
                </div>
                <div class="row">
                            <!-- Featured Video Posts Slide -->
                    @foreach($postseco as $postsec)
                    <div class="col-12 col-lg-6">
                        <div class="single-blog-post d-flex style-3 mb-30">
                            <div class="post-thumbnail">
                                <img src="storage/{{$postsec->photo}}  " alt="">
                            </div>
                            <div class="post-content">
                                <a href="post/{{$postsec->slug}}" class="post-title">{{$postsec->title}}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach()

                        </div>


            <!-- Most Viewed Videos -->
      

                         <div class="most-viewed-videos mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>sport</h5>
                </div>

                <div class="most-viewed-videos-slide owl-carousel">

                    <!-- Single Blog Post -->
                    @foreach($postslife as $postslif)
                    <div class="single-blog-post style-4">
                        <div class="post-thumbnail">
                            <img src="/storage/{{$postslif->photo}}" alt="">
                            <a href="post/{{$postslif->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                            <span class="video-duration">{{$postslif->title}}</span>
                        </div>
                    </div>
                    @endforeach()
                </div>
            </div>

            <!-- Sports Videos -->
            <div class="sports-videos-area">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>art  </h5>
                </div>

                <div class="sports-videos-slides owl-carousel mb-30">
                    <!-- Single Featured Post -->
                    @foreach($postsart as $postsar)
                    <div class="single-featured-post">
                        <!-- Thumbnail -->
                        <div class="post-thumbnail mb-50">
                            <img src="/storage/{{$postsar->photo}}" alt="">
                            <a href="post/{{$postsar->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                        </div>
                        <!-- Post Contetnt -->
                        <div class="post-content">
                            <div class="post-meta">
                                <a href="#">{{$postsar->created_at->diffForHumans()}}</a>
                                <a href="/categories/{{$postsar->category->id}}">{{$postsar->category->name}}</a>
                            </div>
                            <p>{{str_limit(strip_tags($postsar->body) ,100)}}</p>
                                    <a class="btn btn-outline-success" href="/post/{{$postsar->slug}}">see more <i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                @endforeach()
                </div>
            </div>





</dvv>

                         <div class="most-viewed-videos mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>life style  </h5>
                </div>

                <div class="most-viewed-videos-slide owl-carousel">

                    <!-- Single Blog Post -->
                    @foreach($postslife as $postslif)
                    <div class="single-blog-post style-4">
                        <div class="post-thumbnail">
                            <img src="/storage/{{$postslif->photo}}" alt="">
                            <a href="post/ar/{{$postslif->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                            <span class="video-duration">{{$postslif->title}}</span>
                        </div>
                    </div>
                    @endforeach()
                </div>
            </div>


                  <div class="feature-video-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 >woman   </h5>
                </div>

                <div class="featured-video-posts">
                    <div class="row">
                        <div class="col-6 col-lg-6">
                            <!-- Single Featured Post -->
                            <div class="single-featured-post">
                                <!-- Thumbnail -->
                                @foreach($postswomen as $postswome)
                                <div class="post-thumbnail mb-50">
                                    <img src="storage/{{$postswome->photo}}" alt="">
                                </div>
                                <!-- Post Contetnt -->
                                <div class="post-content">
                                    <div class="post-meta">
                                        <a href="#">{{$postswome->created_at->diffForHumans()}}</a>
                                        <a href="/categories/{{optional($postswome->category)->id}}">{{optional($postswome->category)->name}}</a>
                                    </div>
                                    <a href="/post/{{$postswome->slug}}">{{$postswome->slug }}</a>
                                    <p>{{str_limit(strip_tags($postswome->body) ,100)}}</p>
                                    <a class="btn btn-outline-success" href="/post/{{$postswome->slug}}">see more <i class="fa fa-eye"></i></a>
                                </div>
                                <!-- Post Share Area -->
                                @endforeach()

                            </div>
                        </div>
                    </div>
                </div>
            <div class="sports-videos-area">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>Technology    </h5>
                </div>

                <div class="sports-videos-slides owl-carousel mb-30">
                    <!-- Single Featured Post -->
                    @foreach($postechno as $postechn)
                    <div class="single-featured-post">
                        <!-- Thumbnail -->
                        <div class="post-thumbnail mb-50">
                            <img src="/storage/{{$postechn->photo}}" alt="">
                            <a href="post/{{$postechn->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                        </div>
                        <!-- Post Contetnt -->
                        <div class="post-content">
                            <div class="post-meta">
                                <a href="#">{{$postechn->created_at->diffForHumans()}}</a>
                                <a href="/categories/{{$postechn->category->id}}">{{$postechn->category->name}}</a>
                            </div>
                            <p>{{str_limit(strip_tags($postechn->body) ,100)}}</p>
                                    <a class="btn btn-outline-success" href="/post/{{$postechn->slug}}">see more <i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                @endforeach()
                </div>
            </div>
</div>
                        <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 >culture  </h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postculture as $postcultur)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postcultur->photo}}" alt="">
                        <div class="post-content">
                            <a href="/post/{{$postcultur->slug}}" class="post-title">{{$postcultur->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div>
            </div>
  <div class="section-heading">
                    <h5 >Appropriate </h5>
                </div>
                <div class="row">
                            <!-- Featured Video Posts Slide -->
                    @foreach($postappropriate as $postappropriat)
                    <div class="col-12 col-lg-6">
                        <div class="single-blog-post d-flex style-3 mb-30">
                            <div class="post-thumbnail">
                                <img src="storage/{{$postappropriat->photo}}  " alt="">
                            </div>
                            <div class="post-content">
                                <a href="post/{{$postappropriat->slug}}" class="post-title">{{$postappropriat->title}}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach()
                        </div>

  <div class="sports-videos-area">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>opinions       </h5>
                </div>

                <div class="sports-videos-slides owl-carousel mb-30">
                    <!-- Single Featured Post -->
                    @foreach($postarticle as $postarticl)
                    <div class="single-featured-post">
                        <!-- Thumbnail -->
                        <div class="post-thumbnail mb-50">
                            <img src="/storage/{{$postarticl->photo}}" alt="">
                            <a href="post/{{$postarticl->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                        </div>
                        <!-- Post Contetnt -->
                        <div class="post-content">
                            <div class="post-meta">
                                <a href="#">{{$postarticl->created_at->diffForHumans()}}</a>
                                <a href="/ar/categories/{{$postarticl->category->id}}">{{$postarticl->category->name}}</a>
                            </div>
                            <p>{{str_limit(strip_tags($postarticl->body) ,100)}}</p>
                                    <a class="btn btn-outline-success" href="/post/{{$postarticl->slug}}">see more <i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                @endforeach()
                </div>
                      <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 >media      </h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postmedia as $postmedi)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postmedi->photo}}" alt="">
                        <div class="post-content">
                            <a href="/post/{{$postmedi->slug}}" class="post-title">{{$postmedi->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div></div>
            </div>
            </div>


        </div>











        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Right Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area right-sidebar mt-30 mb-30 box-shadow">
            <!-- Sidebar Widget -->
            <div class="single-sidebar-widget p-30">
                <!-- Social Followers Info -->
                <div class="social-followers-info">
                    <!-- Facebook -->
                    <a href="#" class="facebook-fans"><i class="fa fa-facebook"></i> <strong>Follow us</strong></a>
                    <!-- Twitter -->
                    <a href="#" class="twitter-followers"><i class="fa fa-twitter"></i><strong> Follow us</strong></a>
                    <!-- YouTube -->
                    <a href="#" class="youtube-subscribers"><i class="fa fa-youtube"></i><strong> Follow us</strong></a>
                    <!-- Google -->
                    <a href="#" class="google-followers"><i class="fa fa-google-plus"></i><strong> Follow us</strong></a>
                </div>
            </div>

            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->

            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->
        </div>


</section>

@endsection