@extends('layouts.master')

@section('content')

<meta property=“og:image” content='http://www.thelibupost.com/storage/{{$post->photo}}' />
<meta property=”og:image:width” content=”180″ />
<meta property=”og:image:height” content=”110″ />
<meta property="og:image" content="http://www.thelibupost.com/{{$post->title}}" />


    <!-- ##### Hero Area End ##### -->
   <div class="hero-area owl-carousel">
        <!-- Single Blog Post -->
        @foreach($urgposts as $urgpost)
        <div class="hero-blog-post bg-img bg-overlay" style="background-image: url(/storage/{{$urgpost->photo}});">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Post Contetnt -->
                        <div class="post-content text-center">
                            <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                <a href="/post/{{$urgpost->slug}}">{{$urgpost->title}}</a>
                            </div>
                            <a href="/post/{{$urgpost->slug}}" class="post-title" data-animation="fadeInUp" data-delay="300ms"></a>
                            <a href="/post/{{$urgpost->slug}}" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach()
        
    <!-- ##### Hero Area End ##### -->
</div>
   
    <div class="mag-breadcrumb py-5">
        <div class="container">
            <div class="row">
                <div class="col-12">


                        <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                        <div class="blog-thumb mb-30">
                            <img src="/storage/{{$post->photo}}" alt="">
                        </div>

                        <div class="blog-content">
                            <div class="post-meta">
                                <a href="#">{{$post->created_at->diffForHumans()}}</a>
                                <a href="/categories/{{$post->category->id}}">{{$post->category->name}}</a>
                            </div>
                            <h4 class="post-title">{{$post->title}}</h4>
                            <!-- Post Meta -->
                            <div class="post-meta-2">
                                <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 1034</a>
                                <a href="#"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 834</a>
                                <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i> 234</a>
                            </div>

                            <p></p>

                            <div class="row">
                                <div class="col-lg-12" >
                                    <p>{{$post->slug}}</p>
                                    <p>{!!$post->body!!}</p>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <img class="mb-15" src="{{$post->photo}}" alt="">
                                </div>
                            </div>

                            <!-- Like Dislike Share -->
                            <div class="like-dislike-share my-5">
<!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ca69e63cdbcb51e"></script>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox_e2n9"></div>

                <a id="shareBtn"  role="button" tabindex="1" class="at-icon-wrapper at-share-btn fa fa-facebook" style="background-color: rgb(0, 119, 181); border-radius: 14px;"><span class="at4-visually-hidden">Share to Facebook</span><span class="at-icon-wrapper" style="line-height: 20px; height: 20px; width: 20px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-linkedin-2" style="fill: rgb(255, 255, 255); width: 20px; height: 20px;" class="fa fa-facebook"><title id="at-svg-linkedin-2">Facebook</title><g></g></svg></span><span class="at-label" style="font-size: 10.5px; line-height: 20px; height: 20px; color: rgb(255, 255, 255);">Facebook</span></a>


<script>
document.getElementById('shareBtn').onclick = function() {
  FB.ui({
    method: 'share',
    display: 'popup',
    href: 'http://www.thelibupost.com/post/{{$post->slug}}',
  }, function(response){});
}
</script>
                            </div>

                            <!-- Post Author -->
                            <div class="post-author d-flex align-items-center">
                                <div class="post-author-thumb">
                                    <img src="{{$post->user->photo}}" alt="">
                                </div>
                                <div class="post-author-desc pl-4">
                                    <a href="#" class="author-name">{{$post->user->name}}</a>
                                    <p>{{$post->user->description}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                              <!-- Comment Area Start -->
                    <div class="comment_area clearfix bg-white mb-30 p-30 box-shadow">
                        <!-- Section Title -->
                        <div class="section-heading">
                            <h5>COMMENTS</h5>
                        </div>

                        <ol>
                            <!-- Single Comment Area -->
                            <li class="single_comment_area">
                                <!-- Comment Content -->
                                <div class="comment-content d-flex">
                                    <div class="col-12">

                                   <div class="fb-comments" data-href="https://www.facebook.com/TheLibuPost" data-numposts="5"></div>
                                </div>
                            </div>

                </div>
            </div>
        </div>
        </div>
    </li>
</ol>
</div>


@endsection