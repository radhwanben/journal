@extends('layouts.app')

@section('content')

@include('emp.layouts.nav')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        	        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        	<div class="panel panel-success">
			  <div class="panel-heading">edit post</div>
			  <div class="panel-body">
			    <form action="{{route('updatePost',['id' =>$post->id])}}" method="post" enctype="multipart/form-data">
			    	{{ csrf_field() }}
			    	{{ method_field('PUT') }}

				  <div class="form-group col-md-3">
				    <label for="title">Post Title</label>
				    <input type="text" class="form-control" name="title" value="{{$post->title}}">
				  </div>
				  <div class="form-group col-md-3">
				    <label for="Slug">Slug</label>
				    <input type="text" class="form-control" name="slug" value="{{$post->slug}}" >
				  </div>

				   <div class="form-group col-md-3" >
				    <label for="title">categories </label>
				    <select class="form-control form-control-lg" name="id_categories" >
				    	@foreach($categories as $categorie)
					  <option value='{{$categorie["id"]}}' >{{$categorie["name"]}}</option>
					  @endforeach()
					</select>
				  </div>
					
					<div class="form-group col-md-3">

                        <label>Upload Image</label>
                        <div class="input-group">
                            <span class="input-group-btn">
				                <span class="btn btn-default btn-file">
				                    Browse… <input type="file" name="file" id="imgInp">
				                </span>
                            </span>
                            <input type="text" class="form-control" name="file" readonly>
                        </div>
                        <img id='img-upload' />
                    </div>

				  <div class="form-group col-md-12">
				    <label for="exampleInputFile">Post Body</label>
				    <textarea class="form-control" rows="5" name="body">{{$post->body}}</textarea>
				  </div>
				  <button type="submit" class="btn btn-success">Save</button>
				  <a class="btn btn-primary" href="{{route('showenglishposts')}}">back</a>
				</form>
			  </div>
			</div>          
        </div>
    </div>

@endsection
