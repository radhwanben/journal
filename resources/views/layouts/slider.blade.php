    <!-- ##### Hero Area End ##### -->
   <div class="hero-area owl-carousel">
        <!-- Single Blog Post -->
        @foreach($urgposts as $urgpost)
        <div class="hero-blog-post bg-img bg-overlay" style="background-image: url({{asset('$urgpost->photo')}});">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Post Contetnt -->
                        <div class="post-content text-center">
                            <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                <a href="#">{{$urgpost->title}}</a>
                                <a href="archive.html"></a>
                            </div>
                            <a href="video-post.html" class="post-title" data-animation="fadeInUp" data-delay="300ms"></a>
                            <a href="video-post.html" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach()
        
    <!-- ##### Hero Area End ##### -->
</div>
