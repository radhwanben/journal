@extends('layouts.app')

@section('content')

@include('emp.layouts.nav')

    <div class="row">

        <div class="col-md-8 col-md-offset-2">
        	        @if ($errors->any())
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

        	<div class="panel panel-success">
			  <div class="panel-heading" style="text-align: right;">مقال جديد </div>
			  <div class="panel-body">
			    <form action="{{route('storePostAr')}}" method="post" enctype="multipart/form-data">
			    	{{ csrf_field() }}

                    <div class="form-group col-md-3">

                        <label style="text-align: right;">رفع الصور</label>
                        <div class="input-group">
                            <span class="input-group-btn">
				                <span class="btn btn-default btn-file">
				                    Browse… <input type="file" name="file" id="imgInp">
				                </span>
                            </span>
                            <input type="text" class="form-control" name="file" readonly>
                        </div>
                        <img id='img-upload' />
                    </div>

				 <div class="form-group col-md-3" style="text-align: right;">
				    <label for="title">الأقسام </label>
				    <select class="form-control form-control-lg" name="id_categories" >
				    	@foreach($categories as $categorie)
					  <option value="{{$categorie->id}}" >{{$categorie->name}}</option>
					  @endforeach()
					</select>
				  </div>


				  <div class="form-group col-md-3" style="text-align: right;">
				    <label for="Slug">معرف المقال </label>
				    <input type="text" class="form-control" name="slug" style="text-align: right;" placeholder="أدخل معرف المقال ">
				  </div>

				   <div class="form-group col-md-3" style="text-align: right;">
				    <label for="title">عنوان المقال </label>
				    <input type="text" style="text-align: right;" class="form-control" name="title" placeholder="أدخل عنوان المقال ">
				  </div>



				  <div class="form-group col-md-12" style="text-align: right;">
				    <label for="exampleInputFile" >محتوى المقال </label>
				    <textarea class="form-control" rows="5" placeholder="أدخل نص محتوى المقال " style="text-align: right;" name="body"></textarea>
				  </div>
				  <div class="form-group col-md-2" >
				  <button type="submit"style="text-align: right;" class="btn btn-success">حفظ </button>
				  <a class="btn btn-primary" href="{{route('home')}}">إلغاء </a>
				  </div>
				</form>
			  </div>
			</div>          
        </div>
    </div>

@endsection
