@extends('layouts.ar.master')

@section('content')

    <!-- ##### Hero Area End ##### -->
   <div class="hero-area owl-carousel">
        <!-- Single Blog Post -->
        @foreach($urgposts as $urgpost)
        <div class="hero-blog-post bg-img bg-overlay" style="background-image: url('/storage/{{$urgpost->photo}}');">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Post Contetnt -->
                        <div class="post-content text-center">

                            <a href="{{url('post') .'/ar/'. $urgpost->slug}}" class="post-title" data-animation="fadeInUp" data-delay="300ms">{{$urgpost->title}}</a>
                            <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                <a href="/ar/categories/{{optional($urgpost->categoryar)->id}}">{{optional($urgpost->categoryar)->name}}</a>
                            </div>
                            <a href="{{url('post') .'/ar/'. $urgpost->slug}}" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach()

    </div>
    <!-- ##### Hero Area End ##### -->
</div>


   

    <!-- ##### Mag Posts Area Start ##### -->
    <section class="mag-posts-area d-flex flex-wrap">
        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Left Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area left-sidebar mt-30 mb-30 bg-white box-shadow">
            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->

        </div>



        <!-- >>>>>>>>>>>>>>>>>>>>
             Main Posts Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="mag-posts-content mt-30 mb-30 p-30 box-shadow">
            <div class="feature-video-posts mb-30">
                <!-- Section Title -->
            <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 style="text-align: right;">ليبيا</h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postlibiya as $postlibiy)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postlibiy->photo}}" alt="">
                        <div class="post-content">
                            <a href="{{url('post') .'/ar/'. $postlibiy->slug}}" class="post-title">{{$postlibiy->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div>
            </div>
            <!-- Trending Now Posts Area -->
            <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 style="text-align: right;">العالم العربي   </h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postsnewsar as $postsnewsa)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postsnewsa->photo}}" alt="">
                        <div class="post-content">
                            <a href="{{url('post') .'/ar/'. $postsnewsa->slug}}" class="post-title">{{$postsnewsa->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div>
            </div>

            <!-- Feature Video Posts Area -->
            <!-- Feature Video Posts Area -->
            
            <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 style="text-align: right;">أخبار العالم </h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postsnews as $postsnew)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postsnew->photo}}" alt="">
                        <div class="post-content">
                            <a href="{{url('post') .'/ar/'. $postsnew->slug}}" class="post-title">{{$postsnew->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div>
            </div>

</div>                
                <div class="section-heading">
                    <h5 style="text-align: right;">إقتصاد </h5>
                </div>
                <div class="row">
                            <!-- Featured Video Posts Slide -->
                    @foreach($postseco as $postsec)
                    <div class="col-12 col-lg-6">
                        <div class="single-blog-post d-flex style-3 mb-30">
                            <div class="post-thumbnail">
                                <img src="storage/{{$postsec->photo}}  " alt="">
                            </div>
                            <div class="post-content">
                                <a href="post/ar/{{$postsec->slug}}" class="post-title">{{$postsec->title}}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach()

                        </div>

                  <div class="section-heading">
                    <h5 style="text-align: right;">رياضة </h5>
                </div>
                <div class="row">
                            <!-- Featured Video Posts Slide -->
                    @foreach($postsport as $postspor)
                    <div class="col-12 col-lg-6">
                        <div class="single-blog-post d-flex style-3 mb-30">
                            <div class="post-thumbnail">
                                <img src="storage/{{$postspor->photo}}  " alt="">
                            </div>
                            <div class="post-content">
                                <a href="post/ar/{{$postspor->slug}}" class="post-title">{{$postspor->title}}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach()

                        </div>


            <!-- Most Viewed Videos -->
      

                         <div class="most-viewed-videos mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>لايف ستيل</h5>
                </div>

                <div class="most-viewed-videos-slide owl-carousel">

                    <!-- Single Blog Post -->
                    @foreach($postslife as $postslif)
                    <div class="single-blog-post style-4">
                        <div class="post-thumbnail">
                            <img src="/storage/{{$postslif->photo}}" alt="">
                            <a href="post/ar/{{$postslif->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                            <span class="video-duration">{{$postslif->title}}</span>
                        </div>
                    </div>
                    @endforeach()
                </div>
            </div>

            <!-- Sports Videos -->
            <div class="sports-videos-area">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>فن </h5>
                </div>

                <div class="sports-videos-slides owl-carousel mb-30">
                    <!-- Single Featured Post -->
                    @foreach($postsart as $postsar)
                    <div class="single-featured-post">
                        <!-- Thumbnail -->
                        <div class="post-thumbnail mb-50">
                            <img src="/storage/{{$postsar->photo}}" alt="">
                            <a href="post/ar/{{$postsar->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                        </div>
                        <!-- Post Contetnt -->
                        <div class="post-content">
                            <div class="post-meta">
                                <a href="#">{{$postsar->created_at->diffForHumans()}}</a>
                                <a href="archive.html">{{$postsar->categoryar->name}}</a>
                            </div>
                            <p>{{str_limit(strip_tags($postsar->body) ,100)}}</p>
                                    <a class="btn btn-outline-success" href="/post/{{$postsar->slug}}">see more <i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                @endforeach()
                </div>
            </div>





</dvv>
                  <div class="feature-video-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 style="text-align: right;">مرأة </h5>
                </div>

                <div class="featured-video-posts">
                    <div class="row">
                        <div class="col-6 col-lg-6">
                            <!-- Single Featured Post -->
                            <div class="single-featured-post">
                                <!-- Thumbnail -->
                                @foreach($postswomen as $postswome)
                                <div class="post-thumbnail mb-50">
                                    <img src="storage/{{$postswome->photo}}" alt="">
                                </div>
                                <!-- Post Contetnt -->
                                <div class="post-content">
                                    <div class="post-meta">
                                        <a href="#">{{$postswome->created_at->diffForHumans()}}</a>
                                        <a href="ar/categories/{{optional($postswome->categoryar)->id}}">{{optional($postswome->categoryar)->name}}</a>
                                    </div>
                                    <a href="/post/ar/{{$postswome->slug}}">{{$postswome->slug }}</a>
                                    <p>{{str_limit(strip_tags($postswome->body) ,100)}}</p>
                                    <a class="btn btn-outline-success" href="/post/ar/{{$postswome->slug}}">see more <i class="fa fa-eye"></i></a>
                                </div>
                                <!-- Post Share Area -->
                                @endforeach()

                            </div>
                        </div>
                    </div>
                </div>
            <div class="sports-videos-area">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>تكنولوجية  </h5>
                </div>

                <div class="sports-videos-slides owl-carousel mb-30">
                    <!-- Single Featured Post -->
                    @foreach($postechno as $postechn)
                    <div class="single-featured-post">
                        <!-- Thumbnail -->
                        <div class="post-thumbnail mb-50">
                            <img src="/storage/{{$postechn->photo}}" alt="">
                            <a href="post/ar/{{$postechn->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                        </div>
                        <!-- Post Contetnt -->
                        <div class="post-content">
                            <div class="post-meta">
                                <a href="#">{{$postechn->created_at->diffForHumans()}}</a>
                                <a href="/ar/categories/{{$postechn->categoryar->id}}">{{$postechn->categoryar->name}}</a>
                            </div>
                            <p>{{str_limit(strip_tags($postechn->body) ,100)}}</p>
                                    <a class="btn btn-outline-success" href="/post/{{$postechn->slug}}">see more <i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                @endforeach()
                </div>
            </div>

                        <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 style="text-align: right;">ثقافة   </h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postculture as $postcultur)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postcultur->photo}}" alt="">
                        <div class="post-content">
                            <a href="{{url('post') .'/ar/'. $postcultur->slug}}" class="post-title">{{$postcultur->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div>
            </div>
  <div class="section-heading">
                    <h5 style="text-align: right;">متفرقات </h5>
                </div>
                <div class="row">
                            <!-- Featured Video Posts Slide -->
                    @foreach($postarticle as $postarticl)
                    <div class="col-12 col-lg-6">
                        <div class="single-blog-post d-flex style-3 mb-30">
                            <div class="post-thumbnail">
                                <img src="storage/{{$postarticl->photo}}  " alt="">
                            </div>
                            <div class="post-content">
                                <a href="post/ar/{{$postarticl->slug}}" class="post-title">{{$postarticl->title}}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach()
                        </div>

  <div class="sports-videos-area">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>مقالات   </h5>
                </div>

                <div class="sports-videos-slides owl-carousel mb-30">
                    <!-- Single Featured Post -->
                    @foreach($postarticle as $postarticl)
                    <div class="single-featured-post">
                        <!-- Thumbnail -->
                        <div class="post-thumbnail mb-50">
                            <img src="/storage/{{$postarticl->photo}}" alt="">
                            <a href="post/ar/{{$postarticl->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                        </div>
                        <!-- Post Contetnt -->
                        <div class="post-content">
                            <div class="post-meta">
                                <a href="#">{{$postarticl->created_at->diffForHumans()}}</a>
                                <a href="/ar/categories/{{$postarticl->categoryar->id}}">{{$postarticl->categoryar->name}}</a>
                            </div>
                            <p>{{str_limit(strip_tags($postarticl->body) ,100)}}</p>
                                    <a class="btn btn-outline-success" href="/post/{{$postarticl->slug}}">see more <i class="fa fa-eye"></i></a>
                        </div>
                    </div>
                @endforeach()
                </div>
                      <div class="trending-now-posts mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5 style="text-align: right;">ميديا    </h5>
                </div>

                <div class="trending-post-slides owl-carousel">
                    <!-- Single Trending Post -->
                    @foreach($postmedia as $postmedi)
                    <div class="single-trending-post">
                        <img src="/storage/{{$postmedi->photo}}" alt="">
                        <div class="post-content">
                            <a href="{{url('post') .'/ar/'. $postmedi->slug}}" class="post-title">{{$postmedi->title}}</a>
                        </div>
                    </div>
                    @endforeach()
            
                </div></div>
            </div>
            </div>


        </div>











        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Right Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="post-sidebar-area right-sidebar mt-30 mb-30 box-shadow">
            <!-- Sidebar Widget -->
            <div class="single-sidebar-widget p-30">
                <!-- Social Followers Info -->
                <div class="social-followers-info">
                    <!-- Facebook -->
                    <a href="https://www.facebook.com/TheLibuPost/" target="_blank" class="facebook-fans"><i class="fa fa-facebook"></i><strong dir="rtl">تابعونا </strong></a>
                    <!-- Twitter -->
                    <a href="#" class="twitter-followers"><i class="fa fa-twitter"></i>  <strong dir="rtl">تابعونا</strong></a>
                    <!-- YouTube -->
                    <a href="#" class="youtube-subscribers"><i class="fa fa-youtube"></i> <strong dir="rtl">تابعونا</strong></a>
                    <!-- Google -->
                    <a href="#" class="google-followers"><i class="fa fa-google-plus"></i>  <strong dir="rtl">تابعونا</strong></a>
                </div>
            </div>

            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->

            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->
        </div>


</section>

@endsection