@extends('layouts.ar.master')

@section('content')

<meta property=“og:image” content='http://www.thelibupost.com/storage/{{$post->photo}}' />
<meta property=”og:image:width” content=”180″ />
<meta property=”og:image:height” content=”110″ />
<meta property="og:image" content="http://www.thelibupost.com/post/ar/{{$post->title}}" />
   <div class="hero-area owl-carousel">
        <!-- Single Blog Post -->
        @foreach($urgposts as $urgpost)
        <div class="hero-blog-post bg-img bg-overlay" style="background-image: url('/storage/{{$urgpost->photo}}');">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Post Contetnt -->
                        <div class="post-content text-center">

                            <a href="{{url('post') .'/ar/'. $urgpost->slug}}" class="post-title" data-animation="fadeInUp" data-delay="300ms">{{$urgpost->title}}</a>
                            <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                <a href="/ar/categories/{{optional($urgpost->categoryar)->id}}">{{optional($urgpost->categoryar)->name}}</a>
                            </div>
                            <a href="{{url('post') .'/ar/'. $urgpost->slug}}" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach()

    </div>
   
    <div class="mag-breadcrumb py-5" dir="rtl" lang="ar">
        <div class="container">
            <div class="row">
                <div class="col-12">



                        <div class="post-details-content bg-white mb-30 p-30 box-shadow">
                        <div class="blog-thumb mb-30">
                            <img src="/storage/{{$post->photo}}" alt="">
                        </div>

                        <div class="blog-content">
                            <div class="post-meta">
                                <a href="/ar/categories/{{$post->categoryar->id}}">{{$post->categoryar->name}}</a>
                                <a href="#">{{$post->created_at->diffForHumans()}}</a>
                            </div>
                            <h4 class="post-title" style="text-align: right;">{{$post->title}}</h4>
                            <!-- Post Meta -->


                            <p></p>

                            <div class="row" style="text-align: right;">
                                <div class="col-lg-12 " >
                                   <p>{{$post->slug}}</p>
                                    <p dir="rtl">{!!$post->body!!}</p>
                                </div>
                            </div>

                            <!-- Like Dislike Share -->
                            <div class="like-dislike-share my-5">

<!-- Go to www.addthis.com/dashboard to customize your tools -->
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ca69e63cdbcb51e"></script>


                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox_e2n9"></div>
                <a id="shareBtn"  role="button" tabindex="1" class="at-icon-wrapper at-share-btn fa fa-facebook" style="background-color: rgb(0, 119, 181); border-radius: 14px;"><span class="at4-visually-hidden">Share to Facebook</span><span class="at-icon-wrapper" style="line-height: 20px; height: 20px; width: 20px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-linkedin-2" style="fill: rgb(255, 255, 255); width: 20px; height: 20px;" class="fa fa-facebook"><title id="at-svg-linkedin-2">Facebook</title><g></g></svg></span><span class="at-label" style="font-size: 10.5px; line-height: 20px; height: 20px; color: rgb(255, 255, 255);">Facebook</span></a>


<script>
document.getElementById('shareBtn').onclick = function() {
  FB.ui({
    method: 'share',
    display: 'popup',
    href: 'http://www.thelibupost.com/post/ar/{{$post->slug}}',
  }, function(response){});
}
</script>                  

                            </div>



                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
            

                            <!-- Post Author -->
                            <div class="post-author d-flex align-items-center">
                                <div class="post-author-thumb">
                                    <img src="{{$post->user->photo}}" alt="">
                                </div>
                                <div class="post-author-desc pl-4">
                                    <a href="#" class="author-name">{{$post->user->name}}</a>
                                    <p>{{$post->user->description}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                              <!-- Comment Area Start -->
                <div class="comment_area clearfix bg-white mb-30 p-30 box-shadow">
                        <!-- Section Title -->
                        <div class="section-heading">
                            <h5>COMMENTS</h5>
                        </div>

                        <ol>
                            <!-- Single Comment Area -->
                            <li class="single_comment_area">
                                <!-- Comment Content -->
                                <div class="comment-content d-flex">
                                    <div class="col-12">

                                   <div class="fb-comments" data-href="https://www.facebook.com/TheLibuPost" data-numposts="5"></div>
                                </div>
                            </div>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_follow_toolbox"></div>
                </div>


            
            </div>
        </div>
        </div>
    </li>
</ol>
</div>


@endsection