@extends('layouts.ar.master')

@section('content')

   <div class="hero-area owl-carousel">
        <!-- Single Blog Post -->
        @foreach($urgposts as $urgpost)
        <div class="hero-blog-post bg-img bg-overlay" style="background-image: url(/storage/{{$urgpost->photo}});">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <!-- Post Contetnt -->
                        <div class="post-content text-center">
                            <div class="post-meta" data-animation="fadeInUp" data-delay="100ms">
                                <a href="/post/ar/{{$urgpost->slug}}">{{$urgpost->title}}</a>
                            </div>
                            <a href="/post/ar/{{$urgpost->slug}}" class="post-title" data-animation="fadeInUp" data-delay="300ms"></a>
                            <a href="/post/ar/{{$urgpost->slug}}" class="video-play" data-animation="bounceIn" data-delay="500ms"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endforeach()
        
    <!-- ##### Hero Area End ##### -->
</div>

    <!-- ##### Mag Posts Area Start ##### -->
    <section class="mag-posts-area d-flex flex-wrap">
        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Left Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->



        <!-- >>>>>>>>>>>>>>>>>>>>
             Main Posts Area
        <<<<<<<<<<<<<<<<<<<<< -->
        <div class="mag-posts-content mt-30 mb-30 p-30 box-shadow" style="margin-left: 28px;">
            <!-- Trending Now Posts Area -->

            <!-- Most Viewed Videos -->
            <div class="most-viewed-videos mb-30">
                <!-- Section Title -->
                <div class="section-heading">
                    <h5>{{$cat->name}}</h5>
                </div>

            <div class="col-12 col-xl-11">
              @foreach($posts as $post)
                        <!-- Single Catagory Post -->
                        <div dir="rtl" class="single-catagory-post d-flex flex-wrap">
                            <!-- Thumbnail -->
                            <div  class="post-thumbnail bg-img" style="background-image: url(/storage/{{$post->photo}});">
                                <a href="/post/ar/{{$post->slug}}" class="video-play"><i class="fa fa-play"></i></a>
                            </div>

                            <!-- Post Contetnt -->
                            <div class="post-content" style="text-align: left;">
                                <div class="post-meta">
                                    <a href="#">{{$post->created_at->diffForHumans()}}}</a>
                                    <a href="/ar/categories/{{$post->categoryar->id}}">{{$post->categoryar->name}}</a>
                                </div>
                                <a href="/post/ar/{{$post->slug}}" class="post-title" dir="rtl">{{$post->title}}</a>
                                <!-- Post Meta -->

                        </div>
                </div>
                    @endforeach()

                <ul class="pagination">
                    <li>{!! $posts->links() !!}</li>
                </ul>
            </div>

        </div>
    </div>



            <div class="post-sidebar-area right-sidebar mt-30 mb-30 box-shadow">
            <!-- Sidebar Widget -->
            <div class="single-sidebar-widget p-30">
                <!-- Social Followers Info -->
                <div class="social-followers-info">
                    <!-- Facebook -->
                    <a href="https://www.facebook.com/TheLibuPost/" target="_blank" class="facebook-fans"><i class="fa fa-facebook"></i><strong dir="rtl">تابعونا </strong></a>
                    <!-- Twitter -->
                    <a href="#" class="twitter-followers"><i class="fa fa-twitter"></i>  <strong dir="rtl">تابعونا</strong></a>
                    <!-- YouTube -->
                    <a href="#" class="youtube-subscribers"><i class="fa fa-youtube"></i> <strong dir="rtl">تابعونا</strong></a>
                    <!-- Google -->
                    <a href="#" class="google-followers"><i class="fa fa-google-plus"></i>  <strong dir="rtl">تابعونا</strong></a>
                </div>
            </div>

            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->

            <!-- Sidebar Widget -->


            <!-- Sidebar Widget -->
        </div>



        <!-- >>>>>>>>>>>>>>>>>>>>
         Post Right Sidebar Area
        <<<<<<<<<<<<<<<<<<<<< -->
 


</section>


@endsection