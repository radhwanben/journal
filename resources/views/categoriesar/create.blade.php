@extends('layouts.app')

@section('content')

<div class="navbar navbar-inverse navbar-fixed-left">
  <a class="navbar-brand" href="#"><i class="fa fa-eye"></i> view site </a>
  <ul class="nav navbar-nav">
   <li><a href="#"><i class="fas fa-user-alt"></i> profile</a></li>
 <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-file"></i>  posts <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ChefShowEnglishPost')}}"><i class="fas fa-trash-alt"></i> English posts</a></li>
      <li><a href="{{route('ShowArPost')}}"><i class="fas fa-trash-alt"></i> Arabic posts</a></li>
     </ul>
   </li>

    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-file"></i>  categories <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ShowEnCategory')}}"><i class="fas fa-trash-alt"></i> English categories</a></li>
      <li><a href="{{route('ShowARCategory')}}"><i class="fas fa-trash-alt"></i> Arabic categories</a></li>
     </ul>
   </li>

   </li>
   
   <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-business-time"></i> activity <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ChefActivesPosts')}}"><i class="fas fa-check"></i> active posts</a></li>
      <li><a href="{{route('ChefPendingsPosts')}}"><i class="far fa-clock"></i> pending posts</a></li>
      <li><a href="{{route('ChefDeletedsPosts')}}"><i class="fas fa-trash-alt"></i> deleted posts</a></li>
     </ul>
   </li>
  </ul>
</div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2" >
        	<div class="panel panel-success" style="text-align: right;">
			  <div class="panel-heading" style="text-align: right;">إضافة مجموعة جديدة </div>
			  <div class="panel-body" >
			    <form action="{{route('storeCategoryar')}}" method="post">
			    	{{ csrf_field() }}
				  <div class="form-group col-md-4" style="float: right;">
				    <label for="title" style="text-align: right;">إسم المجموعة </label>
				    <input type="text" class="form-control" name="name" placeholder="إضافة مجموعة  جديدة " style="text-align: right;">
				  </div>
				  <div class="form-group col-md-11">
				  <button type="submit" class="btn btn-success" style="text-align: right;">إضافة </button>
				  <a class="btn btn-primary" href="{{route('home')}}" style="text-align: right;">رجوع </a>
				  </div>
				</form>
			  </div>
			</div>          
        </div>
    </div>

@endsection


