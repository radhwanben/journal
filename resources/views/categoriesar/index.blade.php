@extends('layouts.app')

@section('content')

<div class="navbar navbar-inverse navbar-fixed-left">
  <a class="navbar-brand" href="#"><i class="fa fa-eye"></i> view site </a>
  <ul class="nav navbar-nav">
   <li><a href="#"><i class="fas fa-user-alt"></i> profile</a></li>
 <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-file"></i>  posts <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ChefShowEnglishPost')}}"><i class="fas fa-trash-alt"></i> English posts</a></li>
      <li><a href="{{route('ShowArPost')}}"><i class="fas fa-trash-alt"></i> Arabic posts</a></li>
     </ul>
   </li>

    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-file"></i>  categories <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ShowEnCategory')}}"><i class="fas fa-trash-alt"></i> English categories</a></li>
      <li><a href="{{route('ShowARCategory')}}"><i class="fas fa-trash-alt"></i> Arabic categories</a></li>
     </ul>
   </li>

   </li>
   
   <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-business-time"></i> activity <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ChefActivesPosts')}}"><i class="fas fa-check"></i> active posts</a></li>
      <li><a href="{{route('ChefPendingsPosts')}}"><i class="far fa-clock"></i> pending posts</a></li>
      <li><a href="{{route('ChefDeletedsPosts')}}"><i class="fas fa-trash-alt"></i> deleted posts</a></li>
     </ul>
   </li>
  </ul>
</div>


        <div class="col-md-8 col-md-offset-2">
        	  <div class="panel panel-success">
        	  	<a style="float: left; text-align: right;" class="btn btn-small btn-success" href="{{route('createCategoryar')}}">إضافة مجموعة </a>
			  <div class="panel-heading" style="text-align: right;">قائمة المجموعات  </div>
			  <div class="panel-body">
          <table class="table table-border">
          	<tr>
          		<th style="text-align: right;">اعدادت</th>
              <th style="text-align: right;">عدد المقلات </th>
              <th style="text-align: right;">إسم المجموعة </th>

          	</tr>
          	<tr>
          		@foreach($categories as $categorie)
              <td style="text-align: right;"><a class="btn btn-small btn-primary" href="{{url('chef/ar/category/'.$categorie->id.'/edit')}}">تعديل </a></td>
              <td style="text-align: right;">{{count($categorie->postar)}}</td>
          		<td style="text-align: right;">{{$categorie->name}}</td>
          	</tr>
          	@endforeach()
          </table>
          		<a style="float: right; text-align: right;" class="btn btn-small btn-default" href="{{route('home')}}">رجوع </a>
			</div>
		</div>

@endsection
