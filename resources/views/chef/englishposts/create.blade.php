@extends('layouts.app')

@section('content')

<div class="navbar navbar-inverse navbar-fixed-left">
  <a class="navbar-brand" href="#"><i class="fa fa-eye"></i> view site </a>
  <ul class="nav navbar-nav">
   <li><a href="#"><i class="fas fa-user-alt"></i> profile</a></li>
 <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-file"></i>  posts <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ChefShowEnglishPost')}}"><i class="fas fa-trash-alt"></i> English posts</a></li>
      <li><a href="{{route('ShowArPost')}}"><i class="fas fa-trash-alt"></i> Arabic posts</a></li>
     </ul>
   </li>

    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-file"></i>  categories <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ShowEnCategory')}}"><i class="fas fa-trash-alt"></i> English categories</a></li>
      <li><a href="{{route('ShowARCategory')}}"><i class="fas fa-trash-alt"></i> Arabic categories</a></li>
     </ul>
   </li>

   </li>
   
   <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-business-time"></i> activity <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ChefActivesPosts')}}"><i class="fas fa-check"></i> active posts</a></li>
      <li><a href="{{route('ChefPendingsPosts')}}"><i class="far fa-clock"></i> pending posts</a></li>
      <li><a href="{{route('ChefDeletedsPosts')}}"><i class="fas fa-trash-alt"></i> deleted posts</a></li>
     </ul>
   </li>
  </ul>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="panel panel-success">
            <div class="panel-heading">create new post</div>
            <div class="panel-body">
                <form action="{{route('storeChefPost')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group col-md-3">
                        <label for="title">Post Title</label>
                        <input type="text" class="form-control" name="title" placeholder="Write Post Title">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="Slug">Slug</label>
                        <input type="text" class="form-control" name="slug" placeholder="Write Slug">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="title">categories</label>
                        <select class="form-control form-control-lg" name="id_categories">
                            @foreach($categories as $categorie)
                            <option value="{{$categorie->id}}">{{$categorie->name}}</option>
                            @endforeach()
                        </select>
                    </div>
                    <div class="form-group col-md-3">

                        <label>Upload Image</label>
                        <div class="input-group">
                            <span class="input-group-btn">
				                <span class="btn btn-default btn-file">
				                    Browse… <input type="file" name="file" id="imgInp">
				                </span>
                            </span>
                            <input type="text" class="form-control" name="file" readonly>
                        </div>
                        <img id='img-upload' />
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputFile">Post Body</label>
                        <textarea class="form-control" rows="5" placeholder="Write Post" name="body"></textarea>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-success">Create</button>
                        <a class="btn btn-primary" href="{{route('home')}}">back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
