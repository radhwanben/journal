@extends('layouts.app')

@section('content')


<div class="navbar navbar-inverse navbar-fixed-left">
  <a class="navbar-brand" href="#"><i class="fa fa-eye"></i> view site </a>
  <ul class="nav navbar-nav">
   <li><a href="#"><i class="fas fa-user-alt"></i> profile</a></li>
 <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-file"></i>  posts <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ChefShowEnglishPost')}}"><i class="fas fa-trash-alt"></i> English posts</a></li>
      <li><a href="{{route('ShowArPost')}}"><i class="fas fa-trash-alt"></i> Arabic posts</a></li>
     </ul>
   </li>

    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-file"></i>  categories <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ShowEnCategory')}}"><i class="fas fa-trash-alt"></i> English categories</a></li>
      <li><a href="{{route('ShowARCategory')}}"><i class="fas fa-trash-alt"></i> Arabic categories</a></li>
     </ul>
   </li>

   </li>
   
   <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-business-time"></i> activity <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ChefActivesPosts')}}"><i class="fas fa-check"></i> active posts</a></li>
      <li><a href="{{route('ChefPendingsPosts')}}"><i class="far fa-clock"></i> pending posts</a></li>
      <li><a href="{{route('ChefDeletedsPosts')}}"><i class="fas fa-trash-alt"></i> deleted posts</a></li>
     </ul>
   </li>
  </ul>
</div>




        <div class="col-md-8 col-md-offset-2">
        	  <div class="panel panel-success">
        	  	<a style="float: right;" class="btn btn-small btn-success" href="{{route('createChefPost')}}">Create post</a>
			  <div class="panel-heading">List of  posts </div>
			  <div class="panel-body">
          <table class="table table-border">
          	<tr>
              <th>Photo</th>
          		<th>Title</th>
          		<th>Slug</th>
              <th>user</th>
          	</tr>
          	<tr>
          		@foreach($posts as $post)
              <td><img  style="width: 45px"   src="/storage/{{$post->photo}}"></td>

          		<td>{{$post->title}}</td>
          		<td>{{$post->slug}}</td>
              <td>{{$post->user->name}}</td>
          	</tr>
          	@endforeach()
          </table>
          <hr>
          <table class="table table-border">
            <tr>
              <th >الصورة </th>
              <th style="text-align: right;">عنوان المقال </th>
              <th style="text-align: right;">معرف المقال </th>
            </tr>
            <tr>
              @foreach($arposts as $arpost)
              <td><img  style="width: 45px"   src="/storage/{{$arpost->photo}}"></td>

              <td style="text-align: right;">{{$arpost->title}}</td>
              <td style="text-align: right;">{{$arpost->slug}}</td>
            </tr>
            @endforeach()
          </table>
          		<a style="float: right;" class="btn btn-small btn-default" href="{{route('home')}}">Back</a>
			</div>
		</div>
@endsection
