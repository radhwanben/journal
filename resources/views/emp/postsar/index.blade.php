@extends('layouts.app')

@section('content')

@include('emp.layouts.nav')


        <div class="col-md-8 col-md-offset-2">
        	  <div class="panel panel-success">
        	  	<a style="float: left;" style="text-align: right;" class="btn btn-small btn-success" href="{{route('createPostar')}}">مقال جديد </a>
			  <div class="panel-heading" style="text-align: right;">قائمة المقالات  </div>
			  <div class="panel-body">
          <table class="table table-border">
          	<tr>
          		<th style="text-align: right;" colspan="2px">إعدادات</th>
              <th style="text-align: right;">محتوى المقال</th>
              <th style="text-align: right;">معرف المقال</th>
              <th style="text-align: right;">عنوان المقال</th>
              <th style="text-align: right;">الصورة </th>
          	</tr>
          	<tr>
          		@foreach($posts as $post)
              <td>
                <form action="{{route('DeleteArPost',['id'=>$post->id])}}" method="POST">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                  <input class="btn btn-danger" value="حذف" type="submit" onclick="return confirm('هل تريد حقاً حذف هذا المقال  ?');">
                </form>
              </td>
                            <td><a class="btn btn-small btn-primary" href="{{url('employer/post/ar/'.$post->id.'/edit')}}">تعديل </a></td>
              
              <td style="text-align: right;">{{str_limit(strip_tags($post->body) ,10)}}</td>
              <td style="text-align: right;">{{$post->slug}}</td>
              <td style="text-align: right;">{{$post->title}}</td>
              <td style="text-align: right;"><img  style="width: 45px"   src="/storage/{{$post->photo}}"></td>

          	</tr>
          	@endforeach()
          </table>
          		<a style="float: right; text-align: right;" class="btn btn-small btn-default" href="{{route('home')}}">رجوع </a>
			</div>
		</div>

@endsection
