
<div class="navbar navbar-inverse navbar-fixed-left">
  <a class="navbar-brand" href="/"><i class="fa fa-eye"></i> view site </a>
  <ul class="nav navbar-nav">
      <li><a href="#"><i class="fas fa-user-alt"></i> profile</a></li>

 <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="far fa-file"></i>  posts <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('showenglishposts')}}"><i class="fas fa-trash-alt"></i> English posts</a></li>
      <li><a href="{{route('showarabicpost')}}"><i class="fas fa-trash-alt"></i> Arabic posts</a></li>
     </ul>
   </li>

   </li>
   <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fas fa-business-time"></i> activity <span class="caret"></span></a>
     <ul class="dropdown-menu" role="menu">
      <li><a href="{{route('ActivePosts')}}"><i class="fas fa-check"></i> active posts</a></li>
      <li><a href="{{route('PendingPosts')}}"><i class="far fa-clock"></i> pending posts</a></li>
      <li><a href="{{route('DeletedPosts')}}"><i class="fas fa-trash-alt"></i> deleted posts</a></li>
     </ul>
   </li>
  </ul>
</div>