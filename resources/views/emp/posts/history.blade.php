@extends('layouts.app')

@section('content')

@include('emp.layouts.nav')
        <div class="col-md-8 col-md-offset-2">
        	  <div class="panel panel-success">
        	  	<a style="float: right;" class="btn btn-small btn-success" href="{{route('createPost')}}">Create post</a>
			  <div class="panel-heading">List of  posts </div>
			  <div class="panel-body">
          <table class="table table-border">
          	<tr>
              <th>Photo</th>
          		<th>Title</th>
          		<th>Slug</th>
          	</tr>
          	<tr>
          		@foreach($posts as $post)
              <td><img  style="width: 45px"   src="/storage/{{$post->photo}}"></td>

          		<td>{{$post->title}}</td>
          		<td>{{$post->slug}}</td>
          	</tr>
          	@endforeach()
          </table>
          <hr>
          <table class="table table-border">
            <tr>
              <th >الصورة </th>
              <th style="text-align: right;">عنوان المقال </th>
              <th style="text-align: right;">معرف المقال </th>
            </tr>
            <tr>
              @foreach($arposts as $arpost)
              <td><img  style="width: 45px"   src="/storage/{{$arpost->photo}}"></td>

              <td style="text-align: right;">{{$arpost->title}}</td>
              <td style="text-align: right;">{{$arpost->slug}}</td>
            </tr>
            @endforeach()
          </table>
          		<a style="float: right;" class="btn btn-small btn-default" href="{{route('home')}}">Back</a>
			</div>
		</div>
@endsection
