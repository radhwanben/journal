@extends('layouts.app')

@section('content')

@include('emp.layouts.nav')


        <div class="col-md-8 col-md-offset-2">
        	  <div class="panel panel-success">
        	  	<a style="float: right;" class="btn btn-small btn-success" href="{{route('createPost')}}">Create post</a>
			  <div class="panel-heading">List of  posts </div>
			  <div class="panel-body">
          <table class="table table-border">
          	<tr>
              <th>Photo</th>
          		<th>Title</th>
              <th>Slug</th>
          		<th>body</th>
          		<th colspan="2px">Settings</th>
          	</tr>
          	<tr>
          		@foreach($posts as $post)
              <td><img  style="width: 45px"   src="/storage/{{$post->photo}}"></td>
          		<td>{{$post->title}}</td>
          		<td>{{$post->slug}}</td>
              <td>{{str_limit(strip_tags($post->body) ,10)}}</td>
          		<td><a class="btn btn-small btn-primary" href="{{url('employer/post/'.$post->id.'/edit')}}">edit</a></td>
              <td>
                <form action="{{route('DeletePost',['id'=>$post->id])}}" method="POST">
                            {{ csrf_field() }} {{ method_field('DELETE') }}
                  <input class="btn btn-danger" value="delete" type="submit" onclick="return confirm('Are you sure you want to delete this item?');">
                </form>
          		</td>
          	</tr>
          	@endforeach()
          </table>
          		<a style="float: right;" class="btn btn-small btn-default" href="{{route('home')}}">Back</a>
			</div>
		</div>

@endsection
