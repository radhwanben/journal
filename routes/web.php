<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth'], function () {
    Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
    // list all lfm routes here...
});


/**

admin routes

**/


Route::get('/admin',[
	'middleware' =>'admin',
	function(){
	return view('admin.dashboard');
}]);


/*admin users mangements routes*/

Route::get('admin/users',[
	'uses' =>'AdminUsersController@index',
	'as'=>'ShowUsers'

]);


Route::get('admin/users/create',[
	'uses' =>'AdminUsersController@create',
	'as'=>'CreateUsers'

]);

Route::post('admin/users/create',[
	'uses' =>'AdminUsersController@store',
	'as'=>'StoreUsers'

]);


Route::get('admin/users/{id}/edit',[
	'uses' =>'AdminUsersController@edit',
	'as'=>'EditUsers'

]);

Route::put('admin/users/{id}',[
	'uses' =>'AdminUsersController@update',
	'as'=>'updateUsers'

]);


Route::delete('admin/users/{id}',[

	'uses' => 'AdminUsersController@delete',
	'as' =>'DeleteUsers'
]);



/*admin English Posts mangements routes*/

Route::get('admin/english-posts',[
	'uses' =>'AdminpostEnglishController@index',
	'as'=>'ShowEnPosts'

]);



Route::get('admin/english-posts/create',[
	'uses' =>'AdminpostEnglishController@create',
	'as'=>'CreateEnPosts'

]);



Route::post('admin/english-posts/create',[
	'uses' =>'AdminpostEnglishController@store',
	'as'=>'StoreEnPost'

]);



Route::get('admin/english-posts/{id}/edit',[
	'uses' =>'AdminpostEnglishController@edit',
	'as'=>'EditEnPost'

]);


Route::put('admin/english-posts/{id}',[
	'uses' =>'AdminpostEnglishController@update',
	'as'=>'UpdateEnPosts'

]);


Route::delete('admin/english-posts/{id}',[
	'uses' =>'AdminpostEnglishController@delete',
	'as'=>'DeleteEnPosts'

]);

/*admin Arabic Posts mangements routes*/

Route::get('admin/arabic-posts',[
	'uses' =>'AdminpostArabicController@index',
	'as'=>'ShowArPosts'

]);



Route::get('admin/arabic-posts/create',[
	'uses' =>'AdminpostArabicController@create',
	'as'=>'CreateArPosts'

]);



Route::post('admin/arabic-posts/create',[
	'uses' =>'AdminpostArabicController@store',
	'as'=>'StoreArPost'

]);



Route::get('admin/arabic-posts/{id}/edit',[
	'uses' =>'AdminpostArabicController@edit',
	'as'=>'EditArPost'

]);


Route::put('admin/arabic-posts/{id}',[
	'uses' =>'AdminpostArabicController@update',
	'as'=>'UpdateArPosts'

]);

Route::delete('admin/arabic-posts/{id}',[
	'uses' =>'AdminpostArabicController@delete',
	'as'=>'DeleteArPosts'

]);


Route::get('admin/english/category',[

	'uses' => 'AdminEnglishCategoryController@index',
	'as' => 'AdminShowENCategory'

]);


Route::get('admin/english/category/create',[

	'uses' => 'AdminEnglishCategoryController@create',
	'as' => 'AdminCreateENCategory'

]);


Route::post('admin/english/category/create',[

	'uses' => 'AdminEnglishCategoryController@store',
	'as' => 'AdminStoreENCategory'

]);


Route::get('admin/english/category/{id}/edit',[

	'uses' => 'AdminEnglishCategoryController@edit',
	'as' => 'AdminEditENCategory'

]);

Route::put('admin/english/category/{id}',[

	'uses' => 'AdminEnglishCategoryController@update',
	'as' => 'AdminUpdateENCategory'

]);


Route::delete('admin/english/category/{id}',[

	'uses' => 'AdminEnglishCategoryController@delete',
	'as' => 'AdmindeleteENCategory'

]);




Route::get('admin/arabic/category',[

	'uses' => 'AdminArabicCategoryController@index',
	'as' => 'AdminShowARCategory'

]);


Route::get('admin/arabic/category/create',[

	'uses' => 'AdminArabicCategoryController@create',
	'as' => 'AdminCreateARCategory'

]);


Route::post('admin/arabic/category/create',[

	'uses' => 'AdminArabicCategoryController@store',
	'as' => 'AdminStoreARCategory'

]);


Route::get('admin/arabic/category/{id}/edit',[

	'uses' => 'AdminArabicCategoryController@edit',
	'as' => 'AdminEditARCategory'

]);

Route::put('admin/arabic/category/{id}',[

	'uses' => 'AdminArabicCategoryController@update',
	'as' => 'AdminUpdateARCategory'

]);


Route::delete('admin/arabic/category/{id}',[

	'uses' => 'AdminArabicCategoryController@delete',
	'as' => 'AdmindeleteARCategory'

]);
/**

admin routes

**/

/**

Employer routes

**/
Route::get('employer/profile',[
	'uses' =>'ProfilesController@show',
	'as'=>'showProfile'

]);


/** English routes**/

Route::get('employer/post/create',[
	'uses' =>'PostsController@create',
	'as'=>'createPost'

]);

Route::post('employer/post/create',[
	'uses' =>'PostsController@store',
	'as'=>'storePost'

]);

Route::get('employer/post/{id}/edit',[
	'uses' =>'PostsController@edit',
	'as'=>'editPost'

]);

Route::get('employer/post/english',[
	'uses' =>'PostsController@showenglishposts',
	'as'=>'showenglishposts'

]);


Route::put('employer/post/{id}',[
	'uses' =>'PostsController@update',
	'as'=>'updatePost'

]);

Route::delete('employer/post/{id}',[

	'uses' =>'PostsController@delete',
	'as'=>'DeletePost'

]);


Route::get('employer/posts/history/active',[
	'uses'=>'PostsHistorysController@GetActivePosts',
	'as'=>'ActivePosts'
]);


Route::get('employer/posts/history/pending',[
	'uses'=>'PostsHistorysController@GetPendingPosts',
	'as'=>'PendingPosts'
]);

Route::get('employer/posts/history/deleted',[
	'uses'=>'PostsHistorysController@GetDeletedPosts',
	'as'=>'DeletedPosts'
]);




/**Arabic routes**/




Route::get('employer/posts/arabic',[
	'uses' =>'PostarabicController@showarabicpost',
	'as'=>'showarabicpost'

]);

Route::get('employer/post/ar/create',[
	'uses' =>'PostarabicController@create',
	'as'=>'createPostar'

]);

Route::post('employer/post/ar/create',[
	'uses' =>'PostarabicController@store',
	'as'=>'storePostAr'

]);

Route::get('employer/post/ar/{id}/edit',[
	'uses' =>'PostarabicController@edit',
	'as'=>'editPost'

]);


Route::put('employer/post/ar/{id}',[
	'uses' =>'PostarabicController@update',
	'as'=>'updatePostar'

]);

Route::delete('employer/post/ar/{id}',[

	'uses' =>'PostarabicController@delete',
	'as'=>'DeleteArPost'

]);





/** chef routes **/

// english chef routes

Route::get('chef' ,[
	'uses'=>'ChefController@index',
	'as' =>'dashbord'

]);

Route::get('chef/category/english',[
	'uses' =>'CategorysController@index',
	'as'=>'ShowEnCategory'

]);

Route::get('chef/category/create',[
	'uses' =>'CategorysController@create',
	'as'=>'createCategory'

]);

Route::post('/chef/category/create',[
	'uses' =>'CategorysController@store',
	'as'=>'storeCategory'

]);


Route::get('chef/category/{id}/edit',[
	'uses' =>'CategorysController@edit',
	'as'=>'editCategory'

]);

Route::put('chef/category/{id}',[
	'uses' =>'CategorysController@update',
	'as'=>'updateCategory'

]);

/** chef category aabic routes  **/

Route::get('chef/category/arabic',[
	'uses' =>'CategorysArabicController@index',
	'as'=>'ShowARCategory'

]);

Route::get('chef/ar/category/create',[
	'uses' =>'CategorysArabicController@create',
	'as'=>'createCategoryar'

]);

Route::post('/chef/ar/category/create',[
	'uses' =>'CategorysArabicController@store',
	'as'=>'storeCategoryar'

]);


Route::get('chef/ar/category/{id}/edit',[
	'uses' =>'CategorysArabicController@edit',
	'as'=>'editCategoryar'

]);

Route::put('chef/ar/category/{id}',[
	'uses' =>'CategorysArabicController@update',
	'as'=>'updateCategoryar'

]);



Route::get('chef/En/post/',[
	'uses' =>'EnglishPostChefs@index',
	'as'=>'ChefShowEnglishPost'

]);


Route::get('chef/post/create',[
	'uses' =>'EnglishPostChefs@create',
	'as'=>'createChefPost'

]);


Route::post('chef/post/create',[
	'uses' =>'EnglishPostChefs@store',
	'as'=>'storeChefPost'

]);


Route::get('chef/post/{id}/edit',[
	'uses' =>'EnglishPostChefs@edit',
	'as'=>'editPost'

]);


Route::put('chef/post/{id}',[
	'uses' =>'EnglishPostChefs@update',
	'as'=>'ChefupdatePost'

]);




Route::delete('chef/post/{id}',[
 
	'uses' =>'EnglishPostChefs@delete',
	'as'=>'ChefDeletePost'

]);



Route::get('chef/posts/history/active',[
	'uses'=>'ChefActivitys@GetActivePosts',
	'as'=>'ChefActivesPosts'
]);


Route::get('chef/posts/history/pending',[
	'uses'=>'ChefActivitys@GetPendingPosts',
	'as'=>'ChefPendingsPosts'
]);



Route::get('chef/posts/history/deleted',[
	'uses'=>'ChefActivitys@GetDeletedPosts',
	'as'=>'ChefDeletedsPosts'
]);


// arabic chef routes

Route::get('chef/arabic/category',[

	'uses' =>'CategorysArabicController@index',
	'as' => 'ShowArbicCategory'

]);



Route::get('chef/Ar/post/',[
	'uses' =>'ArabicPostChefs@index',
	'as'=>'ShowArPost'

]);


Route::get('chef/post/ar/create',[
	'uses' =>'ArabicPostChefs@create',
	'as'=>'createChefArPost'

]);


Route::post('chef/post/ar/create',[
	'uses' =>'ArabicPostChefs@store',
	'as'=>'storeChefArPost'

]);


Route::get('chef/post/ar/{id}/edit',[
	'uses' =>'ArabicPostChefs@edit',
	'as'=>'ChedArabeditPost'

]);


Route::put('chef/post/ar/{id}',[
	'uses' =>'ArabicPostChefs@update',
	'as'=>'ChedArabupdatePost'

]);

Route::delete('chef/post/ar/{id}',[
 
	'uses' =>'ArabicPostChefs@delete',
	'as'=>'ChedArabDeletePost'

]);


/** end chef routes**/



/**END ARABIC ROUTES**/



/** Visiteur Routes **/


Route::get('/',[
  'uses'=>'EnglishVisteurController@GetTrending',
  'as'=>'dashbord'
]);

Route::get('/post/{slug}',[
	'uses' =>'EnglishVisteurController@GetSinglePost',
	'as'=>'showPost'

]);


Route::get('/categories/{id}',[
	'uses'=>'EnglishVisteurController@GetPosts',
	'as' =>'GetPosts'
]);


Route::get('/post/ar/{slug}',[
	'uses' =>'ArabicVisteurController@GetSinglePost',
	'as'=>'showArPost'

]);

Route::get('/ar',[
	'uses' =>'ArabicVisteurController@GetTrending',
	'as'=>'showArPosts'

]);

Route::get('ar/categories/{id}',[
	'uses'=>'ArabicVisteurController@GetPosts',
	'as' =>'GetPosts'
]);



Route::get('/contact', function () {
    return view('contact');
});


/**END  Visiteur Routes **/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


