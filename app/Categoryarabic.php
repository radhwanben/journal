<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Categoryarabic extends Model
{

    use SoftDeletes;

    protected $fillable=['name','id_user',];
    protected $dates = ['deleted_at'];
    

    public function postar()
    {
    	return $this->hasMany(Postarabic::class,'id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

}
