<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{

    use SoftDeletes;
    
    protected $fillable=['name','id_user',];
    protected $dates = ['deleted_at'];
    

    public function post()
    {
    	return $this->hasMany(Post::class,'id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

}
