<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            if (Auth::check() && Auth::user()->roles == 'admin') {
                return $next($request);
            }
            elseif (Auth::check() && Auth::user()->roles == 'chef') {
                return redirect('/home');
            }
            else {
                return redirect('/');
            }
    }
}
