<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use App\Postarabic;

class ChefActivitys extends Controller
{
    /**Aurh::user()->id == id
    this function will get all active posts and show it in the history blade 
    **/
    
    public function GetActivePosts()
    {
        $posts =Post::whereIn('posted',array('1'))->where('user_id', '=', Auth::user()->id )->get();
    	$arposts =Postarabic::whereIn('posted',array('1'))->where('user_id', '=', Auth::user()->id )->paginate(10);
    	return view('chef.englishposts.history',compact('posts','arposts'));
    }


    /** 
    this function will get all pending posts and show it in the history blade 
    **/

    public function GetPendingPosts()
    {
        $posts =Post::whereIn('posted',array('0'))->where('user_id', '=', Auth::user()->id )->paginate(10);
    	$arposts =Postarabic::whereIn('posted',array('0'))->where('user_id', '=', Auth::user()->id )->paginate(10);
        //dd(Auth::user());
    	return view('chef.englishposts.history',compact('posts','arposts'));
    }

    /**
    this function will get all deleted posts and show it in the history blade 
    **/

    public function GetDeletedPosts()
    {
        $posts =Post::onlyTrashed()->where('user_id', '=', Auth::user()->id )->paginate(10);
        $arposts =Postarabic::onlyTrashed()->where('user_id', '=', Auth::user()->id )->paginate(10);
        return view('chef.englishposts.history',compact('posts','arposts'));
    }
}
