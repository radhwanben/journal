<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryarabicController extends Controller
{
    /**
	this function will show a  arabic categories create form  

    **/
    public function __construct()
    {
    $this->middleware('auth');
    $this->middleware('chef' );

    }

    public function create()
    {
    	return view('categoriesar.create');
    }

    /**
    this function will store a  arabic categorie data in categories table

    then it will redirect user to the dashbord page tell them that the categorie was create successfully  

    **/

    public function store(Request $request)
    {
    	$categorie = new Categoryarabic();
    	$categorie->name=$request->input('name');
    	$categorie->user_id=Auth::user()->id;
    	$categorie->save();
    	
    	return redirect()->back();
    }


    /**
    this function will show a  arabic categorie edit form

    it will use $id to find categorie id who are similar to his id then show the data in this form   

    **/

    public function edit($id)
    {
        $categorie= Categoryarabic::find($id);

        return view('categoriesar.edit' ,compact('categorie'));

    }

    /**
    this function will Update a arabic  categorie data in categories table

    then it will redirect user to the dashbord page tell them that the categorie was Updated successfully  

    **/

        public function update(Request $request ,$id)
    {
        $categorie =Categoryarabic::find($id);
        $categorie->name=$request->input('name');
        $categorie->user_id=Auth::user()->id;
        $categorie->update();
        
        return redirect()->back();
    }
}
