<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Auth;

class EnglishPostChefs extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');
    $this->middleware('chef');
    }




    public function index()
	{
		$posts = Post::all();
        //dd($posts);
		return view('chef.englishposts.index' ,compact('posts'));
	}


    public function create ()
    {
        $categories = Category::all();

    	return view('chef.englishposts.create' ,compact('categories'));

    }


    public function store(Request $request)
    {

        $request->validate([
                'title' => 'required',
                'slug' => 'required',
                'file'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            ]);

        $fileName = null;

        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('storage', $fileName);    
        }

    	$post= New Post();
        $post->title=$request->input('title');
    	$post->slug=$request->input('slug');
    	$post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
    	$post->user_id=Auth::user()->id;
        $post->photo=$fileName;
    	$post->save();
        alert()->message('post create successfully');
    	return redirect()->route('ChefShowEnglishPost');
    }





    public function edit($id)
    {
    	$post= Post::find($id);
        $categories=Category::all();
        //dd($categories);
    	return view('chef.englishposts.edit',compact('post' , 'categories'));
    }


    public function update(Request $request ,$id)
    {
		$post=Post::find($id);
        $post->title=$request->input('title');
        $post->slug=$request->input('slug');
        $post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
        $post->user_id=Auth::user()->id;
        $post->posted=$request->input('posted');
		$post->update();

		return redirect()->route('ChefShowEnglishPost');
    }


    public function delete($id)
    {
        $post =Post::find($id);
        $post->delete();
        return redirect()->route('ChefShowEnglishPost');
    }
}
