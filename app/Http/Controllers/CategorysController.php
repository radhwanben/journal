<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Auth;

class CategorysController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');
    $this->middleware('chef');
    }


    public function index()
    {
        $categories = Category::all();
        return view('categories.index' ,compact('categories'));
    }

    /**
	this function will show a categories create form  

    **/

    public function create()
    {
    	return view('categories.create');
    }

    /**
    this function will store a categorie data in categories table

    then it will redirect user to the dashbord page tell them that the categorie was create successfully  

    **/

    public function store(Request $request)
    {
    	$categorie = new Category();
    	$categorie->name=$request->input('name');
    	$categorie->user_id=Auth::user()->id;
    	$categorie->save();
    	
    	return redirect()->back();
    }


    /**
    this function will show a categorie edit form

    it will use $id to find categorie id who are similar to his id then show the data in this form   

    **/

    public function edit($id)
    {
        $categorie= Category::find($id);

        return view('categories.edit' ,compact('categorie'));

    }

    /**
    this function will Update a categorie data in categories table

    then it will redirect user to the dashbord page tell them that the categorie was Updated successfully  

    **/

        public function update(Request $request ,$id)
    {
        $categorie =Category::find($id);
        $categorie->name=$request->input('name');
        $categorie->user_id=Auth::user()->id;
        $categorie->update();
        
        return redirect()->back();
    }
}
