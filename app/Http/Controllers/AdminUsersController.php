<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminUsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth' );
        $this->middleware('admin');
    }


    public function index()
    {
    	$users = User::all();
    	return view('admin.users.index' , compact('users'));
    }

    public function create ()
    {
    	return view('admin.users.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'file'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',


            ]);
        $fileName = null;


        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('photo/', $fileName);    
        }

    	$user =New User();
    	$user->name=$request->input('name');
    	$user->email=$request->input('email');
    	$user->password=bcrypt($request->input('password'));
        $user->roles=$request->input('role');
        $user->photo=$fileName;
    	$user->save();
    	return redirect()->route('ShowUsers');
    }


    public function edit($id)
    {
    	$user =User::find($id);
    	return view('admin.users.edit' ,compact('user'));
    }



    public function update(Request $request , $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',

            ]);

    	$user =User::find($id);
		$user->name=$request->input('name');
    	$user->email=$request->input('email');
    	$user->password=bcrypt($request->input('password'));
        $user->roles=$request->input('role');
    	$user->save();
    	return redirect()->route('ShowUsers');
    }


    public function delete($id)
    {
        $user=User::find($id);
        if ($user->post->isEmpty()){
            $user->delete();
            return redirect()->back();
        }

        else{

            return redirect()->back()->withErrors(["This User can't be Deleted"]);
        }
    }




}
