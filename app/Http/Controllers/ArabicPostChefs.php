<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Postarabic;
use App\Categoryarabic;
use Auth;
class ArabicPostChefs extends Controller
{
     public function __construct()
    {
    $this->middleware('auth');
    $this->middleware('chef');
    }




    public function index()
	{
		$posts = Postarabic::all();
        //dd($posts);
		return view('chef.arabicposts.index' ,compact('posts'));
	}


    public function create ()
    {
        $categories = Categoryarabic::all();

    	return view('chef.arabicposts.create' ,compact('categories'));

    }


    public function store(Request $request)
    {

        $request->validate([
                'title' => 'required',
                'slug' => 'required',
                'file'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            ]);

        $fileName = null;

        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('storage', $fileName);    
        }

    	$post= New Postarabic();
        $post->title=$request->input('title');
    	$post->slug=$request->input('slug');
    	$post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
    	$post->user_id=Auth::user()->id;
        $post->photo=$fileName;
    	$post->save();
        alert()->message('post create successfully');        	
    	return redirect()->route('ShowArPost');
    }





    public function edit($id)
    {
    	$post= Postarabic::find($id);
        $categories=Categoryarabic::all();
    	return view('chef.arabicposts.edit',compact('post','categories'));
    }


    public function update(Request $request ,$id)
    {
		$post=Postarabic::find($id);
        $post->title=$request->input('title');
        $post->slug=$request->input('slug');
        $post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
        $post->user_id=Auth::user()->id;
        $post->posted=$request->input('posted');
		$post->update();

		return redirect()->route('ShowArPost');
    }

    public function delete($id)
    {
        $post= Postarabic::find($id);
        $post->delete();
        return redirect()->route('ShowArPost');
    }
}
