<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Auth;
use Carbon\Carbon;


class PostsController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth' )->except(['index', 'show']);
        //$this->middleware('employer')->except(['index', 'show']);
    }

    /**
	this function will show every post in posts table

	with paginate of 10 post for each page 

    **/

    public function index()
    {
      $posts= Post::where('created_at', '>', Carbon::now()->subDays(7))->get();	

      $lastposts= Post::where('created_at', '<', Carbon::now()->subDays(1))->get();  
      
      $categories=Category::all();
      
      return view ('posts.index',compact('posts' ,'categories' ,'lastposts'));
    
    }


    /**
    this function will show every post in posts table

    with the user who created  

    **/


    public function showenglishposts()
    {
        $posts = Post::where('user_id', '=', Auth::user()->id )->get();
        return view('emp.posts.index' ,compact('posts'));
    }



    /**
	this function will show a post create form  

    **/

    public function create()
    {   
        $categories = Category::all();
    	return view('posts.create',compact('categories'));
    }

	/**
	this function will store a post data in posts table

	then it will redirect user to the dashbord page tell them that the post was create successfully  

    **/

    public function store(Request $request)
    {
        //dd($request->all());


     $request->validate([
                'title' => 'required',
                'slug' => 'required',
                'file'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            ]);

        $fileName = null;


        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('storage', $fileName);    
        }

       //dd($fileName);

    	$post= new Post();
        $post->title=$request->input('title');
    	$post->slug=$request->input('slug');
    	$post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
    	$post->user_id=Auth::user()->id;
        $post->photo=$fileName;
    	$post->save();
        alert()->message('post create successfully');        	
    	return redirect()->route('showenglishposts');
    }


    /**
	this function will show a post edit form

	it will use $id to find post id who are similar to his id then show the data in this form   

    **/

	public function edit($id)
	{
		$post= Post::find($id);
        $categories=Category::all();

		return view('posts.edit' ,compact('post' ,'categories'));

	}

	/**
	this function will update a post data in posts table

	then it will redirect user to the dashbord page tell them that the post was updated successfully  

    **/


	public function update(Request $request ,$id)
	{
        $request->validate([
                'file'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);

        $fileName = null;
        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('storage', $fileName);    
        }

        //dd($fileName);

		$post=Post::find($id);
        $post->title=$request->input('title');
        $post->slug=$request->input('slug');
        $post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
        $post->user_id=Auth::user()->id;
        $post->photo=$fileName;
		$post->update();

        return redirect()->route('showenglishposts');
		
	}


    public function delete($id)
    {
        $post = Post::destroy($id);
        
        return redirect()->route('showenglishposts');
    }

}
