<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Postarabic;
use App\Categoryarabic;
use Auth;
class AdminpostArabicController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth' );
        $this->middleware('admin');
    }


    public function index()
	{
		$posts = Postarabic::orderBy('created_at','desc')->paginate(10);

		return view('admin.arabicposts.index' ,compact('posts'));
	}




    public function create ()
    {
        $categories = Categoryarabic::all();

    	return view('admin.arabicposts.create' ,compact('categories'));

    }


    public function store(Request $request)
    {
    	$post= New Postarabic();
        $post->title=$request->input('title');
    	$post->slug=$request->input('slug');
    	$post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
    	$post->user_id=Auth::user()->id;
    	$post->save();
        alert()->message('post create successfully');        	
    	return redirect()->route('ShowArPosts');
    }





    public function edit($id)
    {
    	$post= Postarabic::find($id);
       // $categories =Category::find($id)->pluck("name","id")->all();
        $categories =Categoryarabic::all();
    	return view('admin.arabicposts.edit',compact('post','categories'));
    }


    public function update(Request $request ,$id)
    {
		$post=Postarabic::find($id);
        $post->title=$request->input('title');
        $post->slug=$request->input('slug');
        $post->body=$request->input('body');
        $post->posted=$request->input('posted');
        $post->mostreader=$request->input('mostreader');
        $post->urgent=$request->input('urgent');
        $post->notreader=$request->input('notreader');
        $post->mostviews=$request->input('mostviews');
        $post->id_categories=$request->input('id_categories');
		$post->update();

		return redirect()->route('ShowArPosts');
    }

    public function delete($id)
    {
    $post = Postarabic::find($id);
    $post->delete();
    return redirect()->back();
    }
}
