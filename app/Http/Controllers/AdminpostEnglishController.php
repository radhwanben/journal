<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Auth;

class AdminpostEnglishController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth' );
        $this->middleware('admin');
    }


	public function index()
	{
		$posts = Post::orderBy('created_at','desc')->paginate(10);
        //dd($posts);
		return view('admin.englishposts.index' ,compact('posts'));
	}


    public function create ()
    {
        $categories = Category::all();

    	return view('admin.englishposts.create' ,compact('categories'));

    }


    public function store(Request $request)
    {
        $request->validate([
                'title' => 'required',
                'slug' => 'required',
                'file'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            ]);

        $fileName = null;

        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('storage', $fileName);    
        }


       //dd($fileName);

        $post= new Post();
        $post->title=$request->input('title');
        $post->slug=$request->input('slug');
        $post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
        $post->user_id=Auth::user()->id;
        $post->photo=$fileName;
        $post->posted=$request->input('posted');
        $post->mostreader=$request->input('mostreader');
        $post->urgent=$request->input('urgent');
        $post->notreader=$request->input('notreader');
        $post->save();
        alert()->message('post create successfully');           
        return redirect()->route('ShowEnPosts');
    }





    public function edit($id)
    {
    	$post= Post::find($id);
       $categories =Category::all();
       //dd($categories);
    	return view('admin.englishposts.edit',compact('post','categories'));
    }


    public function update(Request $request ,$id)
    {
        $request->validate([
                'file'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);

        $fileName = null;
        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('storage', $fileName);    
        }

        //dd($fileName);

        $post=Post::find($id);
        $post->title=$request->input('title');
        $post->slug=$request->input('slug');
        $post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
        $post->photo=$fileName;
        $post->posted=$request->input('posted');
        $post->mostreader=$request->input('mostreader');
        $post->mostviews=$request->input('mostviews');
        $post->urgent=$request->input('urgent');
        $post->notreader=$request->input('notreader');
        $post->update();

        return redirect()->route('ShowEnPosts');
    }


    public function delete($id)
    {
        $post=Post::find($id);
        $post->delete();
        return redirect()->back();

    }

}
