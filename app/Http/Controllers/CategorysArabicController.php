<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoryarabic;
use Auth;

class CategorysArabicController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');
    $this->middleware('chef' );

    }

    public function index()
    {
        $categories = Categoryarabic::all();
        return view('categoriesar.index' ,compact('categories'));
    }

    /**
	this function will show arabic categories create form  

    **/

    public function create()
    {
    	return view('categoriesar.create');
    }

    /**
    this function will store  arabic categorie data in categories table

    then it will redirect user to the dashbord page tell them that the categorie was create successfully  

    **/

    public function store(Request $request)
    {
    	$categorie = new Categoryarabic();
    	$categorie->name=$request->input('name');
    	$categorie->user_id=Auth::user()->id;
    	$categorie->save();
    	
    	return redirect()->back();
    }


    /**
    this function will show arabic categorie edit form

    it will use $id to find categorie id who are similar to his id then show the data in this form   

    **/

    public function edit($id)
    {
        $categorie= Categoryarabic::find($id);

        return view('categories.edit' ,compact('categorie'));

    }

    /**
    this function will Update arabic categorie data in categories table

    then it will redirect user to the dashbord page tell them that the categorie was Updated successfully  

    **/

        public function update(Request $request ,$id)
    {
        $categorie =Categoryarabic::find($id);
        $categorie->name=$request->input('name');
        $categorie->user_id=Auth::user()->id;
        $categorie->save();
        
        return redirect()->back();
    }


}
