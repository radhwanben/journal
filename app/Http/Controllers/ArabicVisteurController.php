<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoryarabic;
use App\Postarabic;
use Carbon\Carbon;

class ArabicVisteurController extends Controller
{
    public function GetTrending()
    {
       $libiya=Categoryarabic::where('name' ,'=','ليبيا')->pluck('id');

       $postlibiya = Postarabic::where('id_categories' , '=' , $libiya)->whereIn('posted',array('1'))->orderBy('created_at','desc')->whereIn('posted',array('1'))->get();


       $newsar=Categoryarabic::where('name' ,'=','العالم العربي ')->pluck('id');

       $postsnewsar = Postarabic::where('id_categories' , '=' , $newsar)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();


       $news=Categoryarabic::where('name' ,'=','أخبار العالم ')->pluck('id');

       $postsnews = Postarabic::where('id_categories' , '=' , $news)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();


       $eco=Categoryarabic::where('name' ,'=','إقتصاد')->pluck('id');

       $postseco = Postarabic::where('id_categories' , '=' , $eco)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();



       $sport=Categoryarabic::where('name' ,'=','رياضة')->pluck('id');

       $postsport = Postarabic::where('id_categories' , '=' , $sport)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();




       $art=Categoryarabic::where('name' ,'=','فن')->pluck('id');

       $postsart = Postarabic::where('id_categories' , '=' , $art)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();


       $life=Categoryarabic::where('name' ,'=','لايف ستيل')->pluck('id');

       $postslife = Postarabic::where('id_categories' , '=' , $life)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();

       

       $women=Categoryarabic::where('name' ,'=','مرأة ')->pluck('id');

       $postswomen = Postarabic::where('id_categories' , '=' , $women)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();




       $techno=Categoryarabic::where('name' ,'=','تكنولوجية')->pluck('id');

       $postechno = Postarabic::where('id_categories' , '=' , $techno)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();



       $culture=Categoryarabic::where('name' ,'=','ثقافة')->pluck('id');

       $postculture = Postarabic::where('id_categories' , '=' , $culture)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();


       $article=Categoryarabic::where('name' ,'=','مقالات')->pluck('id');

       $postarticle = Postarabic::where('id_categories' , '=' , $article)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();


       $media=Categoryarabic::where('name' ,'=','ميديا')->pluck('id');

       $postmedia = Postarabic::where('id_categories' , '=' , $media)->whereIn('posted',array('1'))->orderBy('created_at','desc')->where('posted',array('1'))->get();
      
      $urgposts = Postarabic::where('urgent' , '=' , '1')->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();

      //dd($postswomen);

      return view ('postsar.index',
        compact('urgposts','postlibiya','postsnewsar','postsnews','postseco','postsport','postsart','postslife','postswomen','postechno','postculture','postarticle','postmedia'));
    
    }


    /**
	this function will show a post in posts table

	with the $id we just take the data of the post with this id  

    **/

    public function GetSinglePost($slug)
    
    {
    	$post = Postarabic::where('slug', $slug)->firstOrFail();
        $urgposts = Postarabic::where('urgent' , '=' , 1)->get();

        //dd($posts);
    	return view('postsar.show' , compact('post','urgposts'));
    }


    public function GetUrgent()
    {
    	$urgposts = PoPostarabicst::where('urgent' , '=' , 1)->get();

    	return view('posts.index' , compact('urgposts'));
    }



    public function GetPosts($id)
    {
        //$lastposts= Postarabic::where('created_at', '<', Carbon::now()->subDays(1))->get();  
      
        $categories=Categoryarabic::all();

        $cat=Categoryarabic::find($id);
        
        $posts = Postarabic::where('id_categories', '=', $id)->orderBy('created_at','desc')->paginate(6);

       $urgposts = Postarabic::where('urgent' , '=' , '1')->get();

        //dd($cat);

        return view('categoriesar.showposts' , compact('posts' ,'cat','categories','urgposts'));

    }
}
