<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use Carbon\Carbon;

class EnglishVisteurController extends Controller

{
    public function GetTrending()
    {
             $libiya=Category::where('name' ,'=','libya')->pluck('id');

       $postlibiya = Post::where('id_categories' , '=' , $libiya)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();


       $newsar=Category::where('name' ,'=','Arab World')->pluck('id');

       $postsnewsar = Post::where('id_categories' , '=' , $newsar)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();


       $news=Category::where('name' ,'=','world')->pluck('id');

       $postsnews = Post::where('id_categories' , '=' , $news)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();


       $eco=Category::where('name' ,'=','Business')->pluck('id');

       $postseco = Post::where('id_categories' , '=' , $eco)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();



       $sport=Category::where('name' ,'=','sport')->pluck('id');

       $postsport = Post::where('id_categories' , '=' , $sport)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();




       $art=Category::where('name' ,'=','art')->pluck('id');

       $postsart = Post::where('id_categories' , '=' , $art)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();


       $life=Category::where('name' ,'=','life style')->pluck('id');

       $postslife = Post::where('id_categories' , '=' , $life)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();

       
       $woman=Category::where('name' ,'=','woman')->pluck('id');

       $postswomen = Post::where('id_categories' , '=' , $woman)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();



       $techno=Category::where('name' ,'=','Technology')->pluck('id');

       $postechno = Post::where('id_categories' , '=' , $techno)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();



       $culture=Category::where('name' ,'=','culture')->pluck('id');

       $postculture = Post::where('id_categories' , '=' , $culture)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();


       $Appropriate=Category::where('name' ,'=','Appropriate')->pluck('id');

       $postappropriate = Post::where('id_categories' , '=' , $Appropriate)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();



       $article=Category::where('name' ,'=','opinions')->pluck('id');

       $postarticle = Post::where('id_categories' , '=' , $article)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();


       $media=Category::where('name' ,'=','media')->pluck('id');

       $postmedia = Post::where('id_categories' , '=' , $media)->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();
      
       $urgposts = Post::where('urgent' , '=' , '1')->whereIn('posted',array('1'))->orderBy('created_at','desc')->get();


      
      
      return view ('posts.index',compact('urgposts','postlibiya','postsnewsar','postsnews','postseco','postsport','postsart','postslife','postswomen','postechno','postculture','postarticle','postmedia','postappropriate'));
    
    }


    /**
	this function will show a post in posts table

	with the $id we just take the data of the post with this id  

    **/

    public function GetSinglePost($slug)
    
    {
    	$post = Post::where('slug', $slug)->firstOrFail();
        $urgposts = Post::where('urgent' , '=' , 1)->get();

        //dd($post);
    	return view('posts.show' , compact('post','urgposts'));
    }


    public function GetUrgent()
    {
    	$urgposts = Post::where('urgent' , '=' , 1)->get();

    	return view('posts.index' , compact('urgposts'));
    }



    public function GetPosts($id)
    
    {
        $lastposts= Post::where('created_at', '<', Carbon::now()->subDays(1))->get();  
      
        $categories=Category::all();

        $cat=Category::find($id);
        
        $posts = Post::where('id_categories', '=', $id)->orderBy('created_at','desc')->paginate(6);

       $urgposts = Post::where('urgent' , '=' , '1')->get();

        //dd($posts);

        return view('categories.showposts' , compact('posts','lastposts' ,'cat','categories','urgposts'));

    }


    public function showCategories()
    {
      $categories =Category::all();
      return view('layouts.footer' ,compact('categories'));
    }

}
