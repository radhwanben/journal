<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoryarabic;
use Auth;

class AdminArabicCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth' );
        $this->middleware('admin');
    }


    public function index()
    {

    $cateories = Categoryarabic::all();

    return view('admin.categories.index' ,compact('cateories'));

    }


    public function create()
    {
    	return view('admin.categories.create');
    }


    public function store(Request $request)
    {
    	     $request->validate([
                'name' => 'required',

            ]);

    	$categories =New Categoryarabic();
    	$categories->name=$request->input('name');
    	$categories->user_id=Auth::user()->id;
    	$categories->save();
    	return redirect()->route('AdminShowARCategory');

    }


    public function edit($id)
    {
    	$categories = Categoryarabic::find($id);

    	return view('admin.categories.edit' ,compact('categories'));
    }


    public function update(Request $request ,$id)
    {
		$request->validate([
                'name' => 'required',

            ]);

    	$categorie =Categoryarabic::find($id);
        $categorie->name=$request->input('name');
        $categorie->user_id=Auth::user()->id;
        $categorie->update();
        
        return redirect()->route('AdminShowARCategory');
    }

    public function delete($id)
    {
    	$categories=Categoryarabic::find($id);
    	$categories->delete();
    	return redirect()->back();
    }
}
