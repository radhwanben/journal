<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Auth;

class AdminEnglishCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth' );
        $this->middleware('admin');
    }


    public function index()
    {

    $cateories = Category::all();

    return view('admin.categories.en.index' ,compact('cateories'));

    }


    public function create()
    {
    	return view('admin.categories.en.create');
    }


    public function store(Request $request)
    {
    	     $request->validate([
                'name' => 'required',

            ]);

    	$categories =New Category();
    	$categories->name=$request->input('name');
    	$categories->user_id=Auth::user()->id;
    	$categories->save();
    	return redirect()->route('AdminShowENCategory');

    }


    public function edit($id)
    {
    	$categories = Category::find($id);

    	return view('admin.categories.en.edit' ,compact('categories'));
    }


    public function update(Request $request ,$id)
    {
		$request->validate([
                'name' => 'required',

            ]);

    	$categorie =Category::find($id);
        $categorie->name=$request->input('name');
        $categorie->user_id=Auth::user()->id;
        $categorie->update();
        
        return redirect()->route('AdminShowENCategory');
    }


    public function delete($id)
    {
    	$categories=Category::find($id);
    	$categories->delete();
    	return redirect()->back();
    }
}
