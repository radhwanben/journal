<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Postarabic;
use App\Categoryarabic;
use Auth;
use Carbon\Carbon;

class PostarabicController extends Controller
{

    public function __construct()
    {
    $this->middleware('auth',['except' => ['showarabic','index']]);
    $this->middleware('employer');
    }


    /**
	this function will show every post in posts table

	with paginate of 10 post for each page 

    **/


    /**
	this function will show a arabic post in posts table

	with the $id we just take the data of the post with this id  

    **/


    public function showarabicpost()
    {
        $posts = Postarabic::where('user_id', '=', Auth::user()->id )->get();
        return view('emp.postsar.index' ,compact('posts'));
    }

    /**
	this function will show a arabic post create form  

    **/

    public function create()
    {   
        $categories = Categoryarabic::all();
    	return view('postsar.create',compact('categories'));
    }

	/**
	this function will store a arabic post data in posts table

	then it will redirect user to the dashbord page tell them that the post was create successfully  

    **/

    public function store(Request $request)
    {
        //dd($request->all());

        //dd(request()->input('file'));

     $request->validate([
                'title' => 'required',
                'slug' => 'required',
                'file'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            ]);

        $fileName = null;


        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('storage', $fileName);    
        }

       // dd($fileName);

        $post= new Postarabic();
        $post->title=$request->input('title');
        $post->slug=$request->input('slug');
        $post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
        $post->user_id=Auth::user()->id;
        $post->photo=$fileName;
        $post->save();
        alert()->message('post create successfully');           
        return redirect()->route('showarabicpost');
    }


    /**
	this function will show a arabic post edit form

	it will use $id to find post id who are similar to his id then show the data in this form   

    **/

	public function edit($id)
	{
		$post= Postarabic::find($id);
		$categories =Categoryarabic::find($post->id_categories)->get();
		//dd($categories);
		return view('postsar.edit' ,compact('post' ,'categories'));

	}

	/**
	this function will update a arabic post data in posts table

	then it will redirect user to the dashbord page tell them that the post was updated successfully  

    **/


	public function update(Request $request ,$id)
	{

     $request->validate([
                'title' => 'required',
                'slug' => 'required',
                'file'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            ]);

        $fileName = null;


        if (request()->hasFile('file')) {
            $file = request()->file('file');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('storage/', $fileName);    
        }



		$post=Postarabic::find($id);
        $post->title=$request->input('title');
        $post->slug=$request->input('slug');
        $post->body=$request->input('body');
        $post->id_categories=$request->input('id_categories');
        $post->user_id=Auth::user()->id;
        $post->photo=$fileName;
		$post->update();

        return redirect()->route('showarabicpost');
		
	}

    public function delete($id)
    {
        $post = Postarabic::destroy($id);
        
        return redirect()->route('showarabicpost');
    }

}
