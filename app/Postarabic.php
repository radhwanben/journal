<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Postarabic extends Model
{

    use SoftDeletes;
    
    protected $fillable=['title','body','id_user','slug',];
    protected $dates = ['deleted_at'];

    public function categoryar()
    {
    	return $this->belongsTo(Categoryarabic::class,'id_categories');
    }


    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
